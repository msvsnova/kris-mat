#------------------------------------------------------------------------------
#
# MAT APPLICATION - MAKEFILE
# By FIST Kresimir Mandic 28.03.1999.
#
#------------------------------------------------------------------------------
#TD           = T:\mat\ 
TD           = X:\mat\ 
FD           = Y:\mat\ 
COMD         = .\lib\matcommon.fle 

# Compiler options
#------------------------------------------------------------------------------
CC          = start /w c:\vscd\bin\nova

CFLAGS      = -w -ereadfgl


### mat_mpsy ###
MKFILES1   =        $(TD)mk_art.fle     $(TD)lm_art.fle     $(TD)mk_sklad.fle \
 $(TD)lm_sklad.fle  $(TD)mkpasswd.fle   $(TD)mat_dd.fle     $(TD)mk_ngod.fle  \
 $(TD)mat.fle        $(TD)arti.fle      $(TD)re_fakt.fle    $(TD)mk_pl_rn.fle  \
 $(TD)matlok.fle    $(TD)mk_podr.fle    $(TD)vr_rabat.fle   $(TD)mv_rnal.fle \
 $(TD)mkcjenik.fle  $(TD)mk_rabko.fle   $(TD)mk_vrnal.fle   $(TD)vd_updtecaj.fle \
 $(TD)mk_rnal.fle   $(TD)robno_dd.fle   $(TD)mk_gruci.fle   $(TD)mat_flds.fle \
 $(TD)lmkrabat.fle  $(TD)lmkrabko.fle   $(TD)lm_gruci.fle   $(TD)lmcjenik.fle \
 $(TD)lvrrabat.fle  $(TD)zimart.fle     $(TD)matdbutl.fle   $(TD)ac_dorade.fle    \
 $(TD)mk_coun.fle   $(TD)mk_kla1.fle    $(TD)rnsfaktzb.fle  $(TD)fla_fill.fle \
 $(TD)mk_kla2.fle   $(TD)lm_rnal.fle    $(TD)zimstav.fle    $(TD)m_familije.fle \
 $(TD)zimprim.fle   $(TD)zimizd.fle     $(TD)mat_pos.fle    $(TD)mkdohart.fle \
 $(TD)l_dohart.fle  $(TD)anti_dat.fle   $(TD)matdbut1.fle   $(TD)l_naldek_ac.fle \
 $(TD)mk_katal.fle  $(TD)lmkkatal.fle   $(TD)mkdohkat.fle   $(TD)mk_ctar.fle  \
 $(TD)mk_dekla.fle  $(TD)l_naldek.fle   $(TD)mkartgen.fle   $(TD)l_cartar.fle \
 $(TD)alfafiat.fle  $(TD)mk_smje.fle    $(TD)rns_dd.fle     $(TD)matperms.fle \
 $(TD)flakat_dd.fle $(TD)flakat_dbf.fle $(TD)farkat_dbf.fle $(TD)flakat02.fle \
 $(TD)cartinfo.fle  $(TD)mk_npl.fle     $(TD)finkat_dbf.fle $(TD)flakat05.fle \
 $(TD)cartgen.fle   $(TD)fladbutl.fle   $(TD)vrmkdok.fle    $(TD)fiattp_dd.fle \
 $(TD)mat_a_dd.fle  $(TD)robno_a_dd.fle $(TD)rns_a_dd.fle   $(TD)flakat_a_dd.fle \
 $(TD)matdbutils.fle $(TD)mkvolksdbf.fle  $(TD)fiattp_ins.fle $(TD)fiat_marcia.fle \
 $(TD)lrnvozkart.fle $(TD)vd_dd.fle       $(TD)vd_marke.fle $(TD)vd_modeli.fle \
 $(TD)vd_sklad.fle   $(TD)mk_rabat.fle    $(TD)aca_acde.fle $(TD)acde.fle  \
 $(TD)acde_stan.fle  $(TD)vd_ackat.fle    $(TD)mktsklkp.fle $(TD)arh_fiatcj.fle    \
 $(TD)rd_cjenvp.fle  $(TD)inspektori.fle  $(TD)gar_dealer.fle $(TD)fiattp_a_dd.fle \
 $(TD)rd_ppar.fle    $(TD)garservisi.fle  $(TD)fiattp_txt.fle $(TD)gar_action.fle \
 $(TD)tip_ppar.fle   $(TD)rns_vehicle.fle $(TD)fndorders.fle $(TD)order_conf.fle  \
 $(TD)fdopune.fle    $(TD)mkt_jm.fle      $(TD)l_mkt_jm.fle  $(TD)mk_rnotp.fle    \
 $(TD)mk_rnizd.fle   $(TD)f01_varexcel.fle $(TD)mkaltskl.fle $(TD)mat_default.fle \
 $(TD)lm_altskl.fle  $(TD)prenum_KP.fle   $(TD)matshknj.fle  $(TD)plargo.fle      \
 $(TD)plargos.fle    $(TD)bc128ab.fle     $(TD)mk_knjiz.fle  $(TD)mkknjbl.fle     \
 $(TD)aca_tempario.fle  $(TD)mk2_sklad.fle $(TD)mat_idx.fle  $(TD)progs_mat.fle   \
 $(TD)vd_modelhomo.fle  $(TD)lvd_izsug.fle $(TD)mat_nzk.fle  $(TD)vdjcdca.fle     \
 $(TD)vdjcdul.fle  $(TD)ucomed.fle $(TD)malop.fle $(TD)mk_vpon.fle $(TD)vd_crediflex.fle \
 $(TD)mk_vpons.fle $(TD)lmk_vpon.fle $(TD)vd_proc.fle $(TD)lvd_proc.fle $(TD)lvd_otkup.fle \
 $(TD)lvd_otkzap.fle $(TD)lvd_finlag.fle  $(TD)vd_kateg.fle  $(TD)vd_izborpod.fle \
 $(TD)mv_dorade.fle $(TD)rns_mob_rn.fle $(TD)rns_mob.fle $(TD)rns_mob_ev.fle  \
 $(TD)mob_rnizd.fle $(TD)mlugovori.fle $(TD)ml_cjenik.fle $(TD)l_ug_cjenik.fle \
 $(TD)mat_rev.fle $(TD)m_trvpmp.fle $(TD)man_katalog.fle $(TD)pr_rnal.fle 

### mat_prom ###
MKFILES2   = $(TD)mk_prim.fle $(TD)mk_prims.fle $(TD)lprimke.fle  $(TD)lotp_din.fle     \
    $(TD)mk_kumul.fle    $(TD)mk_kalk.fle    $(TD)mk_kalks.fle    $(TD)lkalkul.fle      \
    $(TD)mk_izd.fle      $(TD)mk_izds.fle    $(TD)lizdat.fle      $(TD)mk_otp.fle       \
    $(TD)mk_otps.fle     $(TD)lotp.fle       $(TD)mk_msk.fle      $(TD)mk_msks.fle      \
    $(TD)lmsk.fle        $(TD)mk_ps.fle      $(TD)lps.fle         $(TD)mk_neklk.fle     \
    $(TD)lneobrac.fle    $(TD)mk_obr.fle     $(TD)mk_poppr.fle    $(TD)mk_treb.fle      \
    $(TD)lotprn.fle      $(TD)mk_kotp.fle    $(TD)rotprn.fle      $(TD)mkupdcje.fle     \
    $(TD)mk_kotps.fle    $(TD)rmsk.fle       $(TD)mk_mskul.fle    $(TD)lprimke4.fle     \
    $(TD)mk_trebs.fle    $(TD)lmk_treb.fle   $(TD)rmk_treb.fle    $(TD)rns_servisi.fle  \
    $(TD)rizdat.fle      $(TD)rotp.fle       $(TD)rprimke.fle     $(TD)mk_nark.fle      \
    $(TD)mk_narks.fle    $(TD)mk_nard.fle    $(TD)mk_nards.fle    $(TD)mkgetkat.fle     \
    $(TD)mkgetsta.fle    $(TD)lnal_otp.fle   $(TD)l_narkzb.fle    $(TD)lprimke1.fle     \
    $(TD)posprim.fle     $(TD)vrs_oper.fle   $(TD)rns_oper.fle    $(TD)rns_prij.fle     \
    $(TD)rns_vlas.fle    $(TD)rns_voz.fle    $(TD)rns_kola.fle    $(TD)lrn_prij.fle     \
    $(TD)mkvrkpop.fle    $(TD)mk_kpop.fle    $(TD)lrnkolar.fle    $(TD)otp_kpop.fle     \
    $(TD)lrnkolam.fle    $(TD)rns_obr.fle    $(TD)lknj_pop.fle    $(TD)lprimke2.fle     \
    $(TD)rns_zabran.fle  $(TD)malprim.fle    $(TD)mk_mprn.fle     $(TD)mkchkcje.fle     \
    $(TD)mk_kalkps.fle   $(TD)mkprmpvp.fle   $(TD)mkstmpvp.fle    $(TD)lpr_rnal.fle     \
    $(TD)mk_mpupl.fle    $(TD)mprnpreg.fle   $(TD)lmkdntot.fle    $(TD)lnk_rast.fle     \
    $(TD)mat_tecaj.fle   $(TD)mkaddotps.fle  $(TD)mkaddmsks.fle   $(TD)lnd_rast.fle     \
    $(TD)lizd_din.fle    $(TD)lprim_din.fle  $(TD)rn_poslovi.fle  $(TD)rn_predlozak.fle \
    $(TD)mk_zapisnik.fle $(TD)rns_modvoz.fle $(TD)rn_fakart.fle   $(TD)mkprvpmp.fle     \
    $(TD)mkstvpmp.fle    $(TD)popust_mp.fle  $(TD)vd_vozila.fle   $(TD)vd_fndfakt.fle   \
    $(TD)lvoz_otprac.fle $(TD)mk_pscje.fle   $(TD)vd_dodop.fle    $(TD)lvoz_primka.fle  \
    $(TD)lvoz_izdat.fle  $(TD)rns_marke.fle  $(TD)voz_zaliha.fle  $(TD)vozilaobr.fle    \
    $(TD)vd_kartvoz.fle  $(TD)vd_popis.fle   $(TD)mpponzag.fle    $(TD)mpponstav.fle    \
    $(TD)lmppon.fle      $(TD)rmpstar.fle    $(TD)vd_lvozfk.fle   $(TD)mkmpobr.fle      \
    $(TD)rns_fakt.fle    $(TD)vd_dnev.fle    $(TD)gar_zapis.fle   $(TD)vehicleinact.fle \
    $(TD)unosprimke.fle  $(TD)multifak.fle   $(TD)lrnal_IIU.fle   $(TD)addmpprim.fle    \
    $(TD)addprimps.fle   $(TD)rns_operat.fle $(TD)vd_fndprim.fle  $(TD)vd_dodopAC.fle   \
    $(TD)rns_mfakt.fle   $(TD)velprim.fle    $(TD)vd_vozAC.fle    $(TD)vd_uplate.fle    \
    $(TD)lvd_uplate.fle  $(TD)intfakt.fle    $(TD)mk_rekz.fle     $(TD)mk_rekzs.fle     \
    $(TD)mkprmpmp.fle    $(TD)mkstmpmp.fle   $(TD)lmskmpvp.fle    $(TD)rmpr1.fle        \
    $(TD)rtermo_nal.fle $(TD)vdponobr.fle    $(TD)mat_uplate.fle  $(TD)lprimke3.fle     \
    $(TD)lvd_zap.fle     $(TD)rnsrac.fle     $(TD)lmskmpmp.fle    $(TD)mat_uplate.fle   \
    $(TD)lotprnps.fle    $(TD)rns_radnik.fle $(TD)lmpnaldek.fle   $(TD)mpallnal.fle     \
    $(TD)lvoz_otpracAM.fle $(TD)lvd_zapAM.fle $(TD)rnsoperdi.fle  $(TD)mk2_izl.fle      \
    $(TD)mkst2_izl.fle   $(TD)rns_grupe.fle  $(TD)mk2_ul.fle      $(TD)rns_pgrupe.fle   \
    $(TD)vd_vrsleas.fle  $(TD)lrnsplaner_nr.fle $(TD)lmsk_AMEH.fle $(TD)vd_vozAC_nf.fle \
    $(TD)lkalkul_AMEH.fle $(TD)lizdat_car.fle $(TD)lrnsvlas.fle   $(TD)rac_vpmp.fle     \
    $(TD)rac_vpmpst.fle   $(TD)lvoz_racSPC.fle $(TD)vd_msk.fle    $(TD)vdmskst.fle      \
    $(TD)lvd_ulkzap.fle  $(TD)lvd_izkzap.fle  $(TD)lvd_razkzap.fle $(TD)lvd_kzap_car.fle \
    $(TD)lvd_msk.fle $(TD)lvd_zbizkzap.fle    $(TD)vd_popcar.fle  $(TD)extra_nar.fle   \
    $(TD)vd_kontkzap.fle  $(TD)vd_nab.fle     $(TD)vd_dorada.fle   $(TD)vd_rezerva.fle \
    $(TD)lvoz_ponpict.fle $(TD)lvoz_ponpict2.fle $(TD)vd_infponude.fle $(TD)rns_najava.fle \
    $(TD)rn_kontakti.fle  $(TD)lvd_infpon.fle $(TD)lvd_infponp1.fle $(TD)lvd_infponp2.fle \
    $(TD)lrns_kontakt.fle $(TD)lnal_otp_ml.fle $(TD)vd_akcija.fle $(TD)vd_zap_akcija.fle \
    $(TD)vd_leasing.fle   $(TD)vd_leas_file.fle  $(TD)lvoz_pon_dev.fle $(TD)lizdat_dt.fle  \
    $(TD)lvd_zahtjev.fle  $(TD)vd_kartdor.fle   $(TD)lvd_kartdor.fle $(TD)lvd_nab.fle   \
    $(TD)vd_pvmsks.fle $(TD)vd_pvmsk.fle $(TD)lvd_pvmsk.fle  $(TD)vd_postoruc.fle \
    $(TD)vd_salon.fle   $(TD)vd_sklad_dor.fle  $(TD)vd_jamstvo.fle  $(TD)vd_pop_dani.fle \
    $(TD)mk_poppr_hd.fle $(TD)vd_dor_int.fle $(TD)vd_prmp.fle  $(TD)vd_prmp_ppar.fle    \
    $(TD)lvoz_racHONDA.fle $(TD)lvoz_otpHONDA.fle $(TD)lvd_infhonda.fle $(TD)lvd_infhonda1.fle \
    $(TD)mkaddprims.fle $(TD)l_vdcar_raz.fle $(TD)ml_otp.fle $(TD)unosprimke_bt.fle \
    $(TD)leasing.fle $(TD)leasingst.fle $(TD)nk_grupe.fle $(TD)rns_mob_otp.fle $(TD)rns_mob_usl.fle \
    $(TD)vd_podanima.fle $(TD)lrn_prij_ac.fle $(TD)l_otpHONDAfo.fle $(TD)l_racHONDAfo.fle \
    $(TD)l_otpracfo.fle  $(TD)rns_mpupl.fle $(TD)ml_prij.fle $(TD)lrn_prij_ml.fle \
    $(TD)rns_boje.fle    $(TD)altnaz_fakart.fle $(TD)lotprn_r1.fle  $(TD)lotp_hpa.fle  

### mat_izvj ###
MKFILES3   = $(TD)invisp.fle $(TD)mk_viman.fle $(TD)mk_arhin.fle  $(TD)mk_dugvp.fle   \
	$(TD)mk_inisp.fle   $(TD)mk_inv.fle      $(TD)mk_obinv.fle    $(TD)l_nardug.fle   \
	$(TD)mk_kart.fle     $(TD)mk_stay.fle    $(TD)mk_dnev.fle     $(TD)mjtr.fle       \
	$(TD)ispizd.fle      $(TD)lkart.fle      $(TD)lstanje.fle     $(TD)ldnevnik.fle   \
	$(TD)mk_rekap.fle    $(TD)mk_raz.fle     $(TD)mk_pobr.fle     $(TD)mk_razin.fle   \
	$(TD)pregprod.fle    $(TD)mk_rntr.fle    $(TD)lproart.fle     $(TD)lprokup.fle    \
	$(TD)pregnab.fle     $(TD)lnabart.fle    $(TD)lnabdob.fle     $(TD)mk_dobkt.fle   \
	$(TD)lm_dobkt.fle    $(TD)mk_proci.fle   $(TD)mk_izdrn.fle    $(TD)rstanje.fle    \
    $(TD)rkart.fle       $(TD)lmsk_obr.fle   $(TD)lmskpobr.fle    $(TD)lotpkalk.fle   \
    $(TD)mksigkol.fle    $(TD)lsigkol.fle    $(TD)rsigkol.fle     $(TD)mk_liman.fle   \
    $(TD)lmkrekap.fle    $(TD)mk_svekt.fle   $(TD)mpregkup.fle    $(TD)mkimazal.fle   \
    $(TD)l_nard.fle      $(TD)mpregnab.fle   $(TD)lkalkmal.fle    $(TD)lnprkup.fle    \
    $(TD)mk_top.fle      $(TD)lrek_kol.fle   $(TD)l_invlst.fle    $(TD)lotpart.fle    \
    $(TD)lotpkup.fle     $(TD)lprodrab.fle   $(TD)mpregMSK.fle    $(TD)lrd_cjenvp.fle \
    $(TD)lmk_zap.fle     $(TD)faktureRN.fle  $(TD)mk_prmp.fle     $(TD)l_radrnal.fle  \
    $(TD)mk_nardug.fle   $(TD)lprodtip.fle   $(TD)komesto.fle     $(TD)lpc_nard.fle   \
    $(TD)lrnobrlst.fle   $(TD)l_poprn.fle    $(TD)l_m_vrnal.fle   $(TD)l_vrmkdok.fle  \
    $(TD)l_intfakt.fle   $(TD)lrns_fakt.fle  $(TD)l_chg_cje.fle   $(TD)lm_familije.fle \
    $(TD)lmpr_zap.fle    $(TD)lmktsmje.fle   $(TD)lmpr_konlp.fle  $(TD)l_intfaktr.fle \
    $(TD)mk_obrtaj.fle   $(TD)lmk_obrtaj.fle $(TD)l_mprnr1.fle    $(TD)l_matrn.fle    \
    $(TD)lskol2.fle      $(TD)lmk_rekz.fle   $(TD)l_matart.fle    $(TD)r_reexp.fle    \
    $(TD)pack_list.fle   $(TD)r_rac_eur.fle  $(TD)r_rac_sit.fle   $(TD)rns_allrn.fle  \
    $(TD)rns_allup.fle  $(TD)lmk_zamjene.fle $(TD)l_zalart.fle    $(TD)l_zalkup.fle   \
    $(TD)mkinvadd.fle    $(TD)msk_preg.fle   $(TD)rnsplaner.fle   $(TD)l_rnsplaner.fle \
    $(TD)AC_rn_allrek.fle $(TD)nf_otp.fle   $(TD)l_radrnal_AC.fle $(TD)lmsk_rast.fle  \
    $(TD)lmk_rekcij.fle  $(TD)util_ameh.fle  $(TD)mt_invzap.fle   $(TD)mk_prijelaz.fle \
    $(TD)mk_nedovr.fle   $(TD)lrd_cvpde.fle  $(TD)kat_cjenik.fle  $(TD)rnsvlas_AMEH.fle \
    $(TD)rns_mjizvj.fle  $(TD)lvoz_primVA.fle $(TD)mk_nedser.fle  $(TD)vpallnal.fle  \
    $(TD)lvpnaldek.fle   $(TD)reparirani.fle  $(TD)rns_faktmp.fle  $(TD)rns_stfaktmp.fle \
    $(TD)rns_fakppar.fle $(TD)l_rekrnal_AC.fle $(TD)lnabprim.fle    $(TD)lvozracSPC1.fle \
    $(TD)lrns_fakt_ml.fle $(TD)mk_azurotp.fle  $(TD)lrns_zap_ml.fle $(TD)rns_dorada.fle \
    $(TD)mk_najam.fle   $(TD)mk_promnajam.fle  $(TD)mk_najamfak.fle  $(TD)mk_faknajam.fle \
    $(TD)lmk_promnajam.fle $(TD)lmk_najam.fle $(TD)najam_cppar.fle  $(TD)mk_najam_st.fle \
    $(TD)azur_faknajam.fle $(TD)najam_fakture.fle $(TD)mk_natjecaj.fle $(TD)mk_natcjenik.fle \
    $(TD)mk_usteda.fle $(TD)lmk_usteda.fle $(TD)vd_sdok.fle $(TD)lmk_natcjenik.fle \
    $(TD)mk_cjenugo.fle $(TD)mk_cjenppar.fle $(TD)lmk_cjenppar.fle $(TD)stvilicari.fle \
    $(TD)rns_doraraz.fle $(TD)rns_planer.fle $(TD)rns_planst.fle $(TD)rn_tips.fle \
    $(TD)rns_neactpgr.fle $(TD)rns_izborpod.fle $(TD)lrd_primke.fle $(TD)lnajam_zap.fle \
    $(TD)lpprodkup.fle $(TD)lprim_fiat.fle $(TD)lmk_rekzs.fle $(TD)rdrazlike.fle \
    $(TD)l_mkaddpr.fle $(TD)mk_najflote.fle $(TD)lrazlikecd.fle $(TD)ml_kartice.fle \
    $(TD)ml_kartnaj.fle $(TD)ml_kfaknaj.fle $(TD)ml_kservis.fle $(TD)ml_kartall.fle \
    $(TD)ml_kart_old.fle $(TD)lml_kartall.fle $(TD)lml_kartice.fle $(TD)lmskart.fle \
    $(TD)lmskkup.fle $(TD)lfakt_izv.fle $(TD)rns_grfakart.fle $(TD)l_rns_radnik.fle \
    $(TD)lrn_fakart.fle $(TD)karic_mit.fle $(TD)lpv_ispod_nab.fle $(TD)lmob_rn_izvj.fle \
    $(TD)lmk_nabava.fle $(TD)lmk_otprema.fle $(TD)pv_npl.fle $(TD)faktusl_as.fle     \
    $(TD)lfaktusl_as.fle $(TD)lpopiskol.fle $(TD)lrns_faktam.fle $(TD)l_radrnal_am.fle \
    $(TD)lrns_potrosni.fle $(TD)lkalkul_ac.fle 


COMMON  =  mat_dd.ls mat_ddx.ls mat_flds.ls mat_pos.ls mat_uplate.ls mk_art.ls  \
           mk_ctar.ls mk_faknajam.ls mk_kalk.ls mk_npl.ls mk_rnal.ls mk_sklad.ls \
           mkcjenik.ls mkt_jm.ls mkupdcje.ls re_fakt.ls rns_dd.ls \
           rns_ddx.ls rns_servisi.ls rnsrac.ls robno_dd.ls robno_ddx.ls vd_dd.ls \
           vd_ddx.ls bc128ab.ls lkalkmal.ls lkalkul.ls matlok.ls \
           mk_kla2.ls mkpasswd.ls vd_leasing.ls vd_sklad.ls vd_vozila.ls vd_vrsleas.ls \
           mk_kla1.ls


MKFILES4    = $(TD)rns_pfakt.fle $(TD)rns_2fakt.fle $(TD)ameh_ps.fle $(TD)bertol_ps.fle \
    $(TD)mkobmsk2.fle $(TD)lmsk2_obr.fle $(TD)lneomsk2.fle $(TD)mksklmsk2.fle \
    $(TD)mkinvrt.fle $(TD)deal_export.fle 

### deac ###
MKFILES = $(TD)deac.fle $(TD)deac_prot.fle $(TD)deac_prnd.fle  $(TD)aca_temp.fle  \
          $(TD)acatempdd.fle $(TD)deacoff.fle $(TD)deacoffot.fle $(TD)deacoffnd.fle \
          $(TD)acde_stanoff.fle  




all			: $(MKFILES1) $(MKFILES2) $(MKFILES3) $(MKFILES4) 

#########################################################################
#
# Pomocni
#
#########################################################################

$(TD)matdbutl.fle:  matdbutl.lgc
    $(CC) $(CFLAGS) appl=matdbutl dotfle=$(TD)matdbutl.fle

$(TD)matdbut1.fle:  matdbut1.lgc
    $(CC) $(CFLAGS) appl=matdbut1 dotfle=$(TD)matdbut1.fle

## COMMON

mat_dd.ls:  mat_dd.lgc
    $(CC) $(CFLAGS) appl=mat_dd dotfle=.\lib\mat_dd.fle 
    @echo " ">mat_dd.ls

mat_ddx.ls:  mat_ddx.lgc
    $(CC) $(CFLAGS) appl=mat_ddx dotfle=.\lib\mat_dd.fle 
    @echo " ">mat_ddx.ls

mat_flds.ls:  mat_flds.lgc
    $(CC) $(CFLAGS) appl=mat_flds dotfle=.\lib\mat_dd.fle 
    @echo " ">mat_flds.ls

mat_pos.ls:  mat_pos.lgc
    $(CC) $(CFLAGS) appl=mat_pos dotfle=$(COMD)
    @echo " ">mat_pos.ls

mat_uplate.ls:  mat_uplate.lgc
    $(CC) $(CFLAGS) appl=mat_uplate dotfle=$(COMD)
    @echo " ">mat_uplate.ls

mk_art.ls:  mk_art.lgc
    $(CC) $(CFLAGS) appl=mk_art dotfle=$(COMD)
    @echo " ">mk_art.ls

mk_ctar.ls:  mk_ctar.lgc
    $(CC) $(CFLAGS) appl=mk_ctar dotfle=$(COMD)
    @echo " ">mk_ctar.ls

mk_faknajam.ls:  mk_faknajam.lgc
    $(CC) $(CFLAGS) appl=mk_faknajam dotfle=$(COMD)
    @echo " ">mk_faknajam.ls

mk_kalk.ls:  mk_kalk.lgc
    $(CC) $(CFLAGS) appl=mk_kalk dotfle=$(COMD)
    @echo " ">mk_kalk.ls

mk_npl.ls:  mk_npl.lgc
    $(CC) $(CFLAGS) appl=mk_npl dotfle=$(COMD)
    @echo " ">mk_npl.ls

mk_rnal.ls:  mk_rnal.lgc
    $(CC) $(CFLAGS) appl=mk_rnal dotfle=$(COMD)
    @echo " ">mk_rnal.ls

mk_sklad.ls:  mk_sklad.lgc
    $(CC) $(CFLAGS) appl=mk_sklad dotfle=$(COMD)
    @echo " ">mk_sklad.ls

mkcjenik.ls:  mkcjenik.lgc
    $(CC) $(CFLAGS) appl=mkcjenik dotfle=$(COMD)
    @echo " ">mkcjenik.ls

mkt_jm.ls:  mkt_jm.lgc
    $(CC) $(CFLAGS) appl=mkt_jm dotfle=$(COMD)
    @echo " ">mkt_jm.ls

mkupdcje.ls:  mkupdcje.lgc
    $(CC) $(CFLAGS) appl=mkupdcje dotfle=$(COMD)
    @echo " ">mkupdcje.ls

re_fakt.ls:  re_fakt.lgc
    $(CC) $(CFLAGS) appl=re_fakt dotfle=$(COMD)
    @echo " ">re_fakt.ls

rns_dd.ls:  rns_dd.lgc
    $(CC) $(CFLAGS) appl=rns_dd dotfle=.\lib\mat_dd.fle 
    @echo " ">rns_dd.ls

rns_ddx.ls:  rns_ddx.lgc
    $(CC) $(CFLAGS) appl=rns_ddx dotfle=.\lib\mat_dd.fle 
    @echo " ">rns_ddx.ls

rns_servisi.ls:  rns_servisi.lgc
    $(CC) $(CFLAGS) appl=rns_servisi dotfle=$(COMD)
    @echo " ">rns_servisi.ls

rnsrac.ls:  rnsrac.lgc
    $(CC) $(CFLAGS) appl=rnsrac dotfle=$(COMD)
    @echo " ">rnsrac.ls

robno_dd.ls:  robno_dd.lgc
    $(CC) $(CFLAGS) appl=robno_dd dotfle=.\lib\mat_dd.fle 
    @echo " ">robno_dd.ls

robno_ddx.ls:  robno_ddx.lgc
    $(CC) $(CFLAGS) appl=robno_ddx dotfle=.\lib\mat_dd.fle 
    @echo " ">robno_ddx.ls

vd_dd.ls:  vd_dd.lgc
    $(CC) $(CFLAGS) appl=vd_dd dotfle=.\lib\mat_dd.fle 
    @echo " ">vd_dd.ls

vd_ddx.ls:  vd_ddx.lgc
    $(CC) $(CFLAGS) appl=vd_ddx dotfle=.\lib\mat_dd.fle 
    @echo " ">vd_ddx.ls

bc128ab.ls:  bc128ab.lgc
    $(CC) $(CFLAGS) appl=bc128ab dotfle=$(COMD)
    @echo " ">bc128ab.ls

lkalkmal.ls:  lkalkmal.lgc
    $(CC) $(CFLAGS) appl=lkalkmal dotfle=$(COMD)
    @echo " ">lkalkmal.ls

lkalkul.ls:  lkalkul.lgc
    $(CC) $(CFLAGS) appl=lkalkul dotfle=$(COMD)
    @echo " ">lkalkul.ls

matlok.ls:  matlok.lgc
    $(CC) $(CFLAGS) appl=matlok dotfle=$(COMD)
    @echo " ">matlok.ls

mk_kla1.ls:  mk_kla1.lgc
    $(CC) $(CFLAGS) appl=mk_kla1 dotfle=$(COMD)
    @echo " ">mk_kla1.ls

mk_kla2.ls:  mk_kla2.lgc
    $(CC) $(CFLAGS) appl=mk_kla2 dotfle=$(COMD)
    @echo " ">mk_kla2.ls

mkpasswd.ls:  mkpasswd.lgc
    $(CC) $(CFLAGS) appl=mkpasswd dotfle=$(COMD)
    @echo " ">mkpasswd.ls

vd_leasing.ls:  vd_leasing.lgc
    $(CC) $(CFLAGS) appl=vd_leasing dotfle=$(COMD)
    @echo " ">vd_leasing.ls

vd_sklad.ls:  vd_sklad.lgc
    $(CC) $(CFLAGS) appl=vd_sklad dotfle=$(COMD)
    @echo " ">vd_sklad.ls

vd_vozila.ls:  vd_vozila.lgc
    $(CC) $(CFLAGS) appl=vd_vozila dotfle=$(COMD)
    @echo " ">vd_vozila.ls

vd_vrsleas.ls:  vd_vrsleas.lgc
    $(CC) $(CFLAGS) appl=vd_vrsleas dotfle=$(COMD)
    @echo " ">vd_vrsleas.ls

#########################################################################
#
# Baza podataka
#
#########################################################################

$(TD)mat_dd.fle:  mat_dd.lgc
    $(CC) $(CFLAGS) appl=mat_dd dotfle=$(TD)mat_dd.fle
    $(CC) -w genddx ddfile=mat_dd
    $(CC) $(CFLAGS) appl=mat_ddx dotfle=$(TD)mat_ddx.fle

$(TD)robno_dd.fle:  robno_dd.lgc
    $(CC) $(CFLAGS) appl=robno_dd dotfle=$(TD)robno_dd.fle
    $(CC) -w genddx ddfile=robno_dd
    $(CC) $(CFLAGS) appl=robno_ddx dotfle=$(TD)robno_ddx.fle

$(TD)rns_dd.fle:  rns_dd.lgc
    $(CC) $(CFLAGS) appl=rns_dd dotfle=$(TD)rns_dd.fle
    $(CC) -w genddx ddfile=rns_dd
    $(CC) $(CFLAGS) appl=rns_ddx dotfle=$(TD)rns_ddx.fle

$(TD)flakat_dd.fle:  flakat_dd.lgc
    $(CC) $(CFLAGS) appl=flakat_dd dotfle=$(TD)flakat_dd.fle
    $(CC) $(CFLAGS) appl=flakat_dd dotfle=$(TD)kat_cjenik.fle
    $(CC) -w genddx ddfile=flakat_dd
    $(CC) $(CFLAGS) appl=flakat_ddx dotfle=$(TD)flakat_ddx.fle
    $(CC) $(CFLAGS) appl=flakat_ddx dotfle=$(TD)kat_cjenik.fle

$(TD)mat_a_dd.fle:  mat_a_dd.lgc
    $(CC) $(CFLAGS) appl=mat_a_dd dotfle=$(TD)mat_a_dd.fle

$(TD)robno_a_dd.fle:  robno_a_dd.lgc
    $(CC) $(CFLAGS) appl=robno_a_dd dotfle=$(TD)robno_a_dd.fle

$(TD)matdbutils.fle:  matdbutils.lgc
    $(CC) $(CFLAGS) appl=matdbutils dotfle=$(TD)matdbutils.fle
    $(CC) $(CFLAGS) appl=matdbutils dotfle=$(FD)matdbutils.fle

$(TD)rns_a_dd.fle:  rns_a_dd.lgc
    $(CC) $(CFLAGS) appl=rns_a_dd dotfle=$(TD)rns_a_dd.fle

$(TD)flakat_a_dd.fle:  flakat_a_dd.lgc
    $(CC) $(CFLAGS) appl=flakat_a_dd dotfle=$(TD)flakat_a_dd.fle

$(TD)flakat_dbf.fle:  flakat_dbf.lgc
    $(CC) $(CFLAGS) appl=flakat_dbf dotfle=$(TD)flakat_dbf.fle

$(TD)farkat_dbf.fle:  farkat_dbf.lgc
    $(CC) $(CFLAGS) appl=farkat_dbf dotfle=$(TD)farkat_dbf.fle

$(TD)finkat_dbf.fle:  finkat_dbf.lgc
    $(CC) $(CFLAGS) appl=finkat_dbf dotfle=$(TD)finkat_dbf.fle

$(TD)vd_dd.fle:  vd_dd.lgc
    $(CC) $(CFLAGS) appl=vd_dd dotfle=$(TD)vd_dd.fle
    $(CC) -w genddx ddfile=vd_dd
    $(CC) $(CFLAGS) appl=vd_ddx dotfle=$(TD)vd_ddx.fle

$(TD)fiattp_dd.fle:  fiattp_dd.lgc
    $(CC) $(CFLAGS) appl=fiattp_dd dotfle=$(TD)fiattp_dd.fle
    $(CC) -w genddx ddfile=fiattp_dd
    $(CC) $(CFLAGS) appl=fiattp_ddx dotfle=$(TD)fiattp_ddx.fle

$(TD)fiattp_a_dd.fle:  fiattp_a_dd.lgc
    $(CC) $(CFLAGS) appl=fiattp_a_dd dotfle=$(TD)fiattp_a_dd.fle

#########################################################################
#
# Maticni i sistemski programi    
#
#########################################################################

$(TD)mat.fle:  mat.lgc
    $(CC) $(CFLAGS) appl=mat dotfle=$(TD)mat.fle

$(TD)mat_nzk.fle:  mat_nzk.lgc
    $(CC) $(CFLAGS) appl=mat_nzk dotfle=$(TD)mat_nzk.fle

$(TD)arti.fle:  arti.lgc
    $(CC) $(CFLAGS) appl=arti dotfle=$(TD)arti.fle

$(TD)matperms.fle:  matperms.lgc
    $(CC) $(CFLAGS) appl=matperms dotfle=$(TD)matperms.fle

$(TD)mat_flds.fle:  mat_flds.lgc
    $(CC) $(CFLAGS) appl=mat_flds dotfle=$(TD)mat_flds.fle
    $(CC) $(CFLAGS) appl=mat_flds dotfle=$(TD)kat_cjenik.fle

$(TD)mk_art.fle:  mk_art.lgc
    $(CC) $(CFLAGS) appl=mk_art dotfle=$(TD)mk_art.fle

$(TD)lm_art.fle:  lm_art.lgc
    $(CC) $(CFLAGS) appl=lm_art dotfle=$(TD)lm_art.fle
    
$(TD)mk_sklad.fle:  mk_sklad.lgc
    $(CC) $(CFLAGS) appl=mk_sklad dotfle=$(TD)mk_sklad.fle
    
$(TD)mk2_sklad.fle:  mk2_sklad.lgc
    $(CC) $(CFLAGS) appl=mk2_sklad dotfle=$(TD)mk2_sklad.fle

$(TD)mat_idx.fle:  mat_idx.lgc
    $(CC) $(CFLAGS) appl=mat_idx dotfle=$(TD)mat_idx.fle

$(TD)lm_sklad.fle:  lm_sklad.lgc
    $(CC) $(CFLAGS) appl=lm_sklad dotfle=$(TD)lm_sklad.fle
    
$(TD)mk_ngod.fle:  mk_ngod.lgc
    $(CC) $(CFLAGS) appl=mk_ngod dotfle=$(TD)mk_ngod.fle
    
$(TD)matlok.fle:  matlok.lgc
    $(CC) $(CFLAGS) appl=matlok dotfle=$(TD)matlok.fle
    
$(TD)mk_podr.fle:  mk_podr.lgc
    $(CC) $(CFLAGS) appl=mk_podr dotfle=$(TD)mk_podr.fle
    
$(TD)vr_rabat.fle:  vr_rabat.lgc
    $(CC) $(CFLAGS) appl=vr_rabat dotfle=$(TD)vr_rabat.fle
    
$(TD)re_fakt.fle:  re_fakt.lgc
    $(CC) $(CFLAGS) appl=re_fakt dotfle=$(TD)re_fakt.fle
    
$(TD)mk_pl_rn.fle:  mk_pl_rn.lgc
    $(CC) $(CFLAGS) appl=mk_pl_rn dotfle=$(TD)mk_pl_rn.fle
    
$(TD)mkpasswd.fle:  mkpasswd.lgc
    $(CC) $(CFLAGS) appl=mkpasswd dotfle=$(TD)mkpasswd.fle
    
$(TD)mk_rabat.fle:  mk_rabat.lgc
    $(CC) $(CFLAGS) appl=mk_rabat dotfle=$(TD)mk_rabat.fle
    
$(TD)mkcjenik.fle:  mkcjenik.lgc
    $(CC) $(CFLAGS) appl=mkcjenik dotfle=$(TD)mkcjenik.fle
    
$(TD)mk_rabko.fle:  mk_rabko.lgc
    $(CC) $(CFLAGS) appl=mk_rabko dotfle=$(TD)mk_rabko.fle
    
$(TD)mk_vrnal.fle:  mk_vrnal.lgc
    $(CC) $(CFLAGS) appl=mk_vrnal dotfle=$(TD)mk_vrnal.fle
    
$(TD)mk_rnal.fle:  mk_rnal.lgc
    $(CC) $(CFLAGS) appl=mk_rnal dotfle=$(TD)mk_rnal.fle
    
$(TD)pr_rnal.fle:  pr_rnal.lgc
    $(CC) $(CFLAGS) appl=pr_rnal dotfle=$(TD)pr_rnal.fle
    
$(TD)mv_rnal.fle:  mv_rnal.lgc
    $(CC) $(CFLAGS) appl=mv_rnal dotfle=$(TD)mv_rnal.fle
    
$(TD)ac_dorade.fle:  ac_dorade.lgc
    $(CC) $(CFLAGS) appl=ac_dorade dotfle=$(TD)ac_dorade.fle
    
$(TD)mv_dorade.fle:  mv_dorade.lgc
    $(CC) $(CFLAGS) appl=mv_dorade dotfle=$(TD)mv_dorade.fle
    
$(TD)mk_rnotp.fle:  mk_rnotp.lgc
    $(CC) $(CFLAGS) appl=mk_rnotp dotfle=$(TD)mk_rnotp.fle
    
$(TD)mk_rnizd.fle:  mk_rnizd.lgc
    $(CC) $(CFLAGS) appl=mk_rnizd dotfle=$(TD)mk_rnizd.fle
    
$(TD)mk_gruci.fle:  mk_gruci.lgc
    $(CC) $(CFLAGS) appl=mk_gruci dotfle=$(TD)mk_gruci.fle
    
$(TD)lmkrabat.fle:  lmkrabat.lgc
    $(CC) $(CFLAGS) appl=lmkrabat dotfle=$(TD)lmkrabat.fle     
                                         
$(TD)lmkrabko.fle:  lmkrabko.lgc               
    $(CC) $(CFLAGS) appl=lmkrabko dotfle=$(TD)lmkrabko.fle     
    
$(TD)lm_gruci.fle:  lm_gruci.lgc
    $(CC) $(CFLAGS) appl=lm_gruci dotfle=$(TD)lm_gruci.fle
    
$(TD)lmcjenik.fle:  lmcjenik.lgc
    $(CC) $(CFLAGS) appl=lmcjenik dotfle=$(TD)lmcjenik.fle
                                         
$(TD)lvrrabat.fle:  lvrrabat.lgc               
    $(CC) $(CFLAGS) appl=lvrrabat dotfle=$(TD)lvrrabat.fle     
    
$(TD)zimart.fle:  zimart.lgc
    $(CC) $(CFLAGS) appl=zimart dotfle=$(TD)zimart.fle
    
$(TD)zimps.fle:  zimps.lgc
    $(CC) $(CFLAGS) appl=zimps dotfle=$(TD)zimps.fle
    
$(TD)mk_coun.fle:  mk_coun.lgc
    $(CC) $(CFLAGS) appl=mk_coun dotfle=$(TD)mk_coun.fle
    
$(TD)mk_kla1.fle:  mk_kla1.lgc
    $(CC) $(CFLAGS) appl=mk_kla1 dotfle=$(TD)mk_kla1.fle
    
$(TD)mk_kla2.fle:  mk_kla2.lgc
    $(CC) $(CFLAGS) appl=mk_kla2 dotfle=$(TD)mk_kla2.fle
    
$(TD)lm_rnal.fle:  lm_rnal.lgc
    $(CC) $(CFLAGS) appl=lm_rnal dotfle=$(TD)lm_rnal.fle
    
$(TD)zimstav.fle:  zimstav.lgc
    $(CC) $(CFLAGS) appl=zimstav dotfle=$(TD)zimstav.fle
    
$(TD)zimprim.fle:  zimprim.lgc
    $(CC) $(CFLAGS) appl=zimprim dotfle=$(TD)zimprim.fle
    
$(TD)zimizd.fle:  zimizd.lgc
    $(CC) $(CFLAGS) appl=zimizd dotfle=$(TD)zimizd.fle
    
$(TD)mat_pos.fle:  mat_pos.lgc
    $(CC) $(CFLAGS) appl=mat_pos dotfle=$(TD)mat_pos.fle
    
$(TD)mkdohart.fle:  mkdohart.lgc
    $(CC) $(CFLAGS) appl=mkdohart dotfle=$(TD)mkdohart.fle
    
$(TD)l_dohart.fle:  l_dohart.lgc
    $(CC) $(CFLAGS) appl=l_dohart dotfle=$(TD)l_dohart.fle

$(TD)anti_dat.fle:  anti_dat.lgc
    $(CC) $(CFLAGS) appl=anti_dat dotfle=$(TD)anti_dat.fle

$(TD)mk_katal.fle:  mk_katal.lgc
    $(CC) $(CFLAGS) appl=mk_katal dotfle=$(TD)mk_katal.fle

$(TD)lmkkatal.fle:  lmkkatal.lgc
    $(CC) $(CFLAGS) appl=lmkkatal dotfle=$(TD)lmkkatal.fle

$(TD)mkdohkat.fle:  mkdohkat.lgc
    $(CC) $(CFLAGS) appl=mkdohkat dotfle=$(TD)mkdohkat.fle

$(TD)mk_ctar.fle:  mk_ctar.lgc
    $(CC) $(CFLAGS) appl=mk_ctar dotfle=$(TD)mk_ctar.fle

$(TD)mk_dekla.fle:  mk_dekla.lgc
    $(CC) $(CFLAGS) appl=mk_dekla dotfle=$(TD)mk_dekla.fle

$(TD)l_naldek.fle:  l_naldek.lgc
    $(CC) $(CFLAGS) appl=l_naldek dotfle=$(TD)l_naldek.fle

$(TD)l_naldek_ac.fle:  l_naldek_ac.lgc
    $(CC) $(CFLAGS) appl=l_naldek_ac dotfle=$(TD)l_naldek_ac.fle

$(TD)mkartgen.fle:  mkartgen.lgc
    $(CC) $(CFLAGS) appl=mkartgen dotfle=$(TD)mkartgen.fle

$(TD)l_cartar.fle:  l_cartar.lgc
    $(CC) $(CFLAGS) appl=l_cartar dotfle=$(TD)l_cartar.fle

$(TD)mk_smje.fle:  mk_smje.lgc
    $(CC) $(CFLAGS) appl=mk_smje dotfle=$(TD)mk_smje.fle

$(TD)mktsklkp.fle:  mktsklkp.lgc
    $(CC) $(CFLAGS) appl=mktsklkp dotfle=$(TD)mktsklkp.fle

$(TD)fla_fill.fle:  fla_fill.lgc
    $(CC) $(CFLAGS) appl=fla_fill dotfle=$(TD)fla_fill.fle

$(TD)flakat02.fle:  flakat02.lgc
    $(CC) $(CFLAGS) appl=flakat02 dotfle=$(TD)flakat02.fle

$(TD)flakat05.fle:  flakat05.lgc
    $(CC) $(CFLAGS) appl=flakat05 dotfle=$(TD)flakat05.fle

$(TD)mk_npl.fle:  mk_npl.lgc
    $(CC) $(CFLAGS) appl=mk_npl dotfle=$(TD)mk_npl.fle

$(TD)vd_marke.fle:  vd_marke.lgc
    $(CC) $(CFLAGS) appl=vd_marke dotfle=$(TD)vd_marke.fle

$(TD)vd_modeli.fle:  vd_modeli.lgc
    $(CC) $(CFLAGS) appl=vd_modeli dotfle=$(TD)vd_modeli.fle

$(TD)vd_modelhomo.fle:  vd_modelhomo.lgc
    $(CC) $(CFLAGS) appl=vd_modelhomo dotfle=$(TD)vd_modelhomo.fle

$(TD)lvd_izsug.fle:  lvd_izsug.lgc
    $(CC) $(CFLAGS) appl=lvd_izsug dotfle=$(TD)lvd_izsug.fle

$(TD)vdjcdca.fle:  vdjcdca.lgc
    $(CC) $(CFLAGS) appl=vdjcdca dotfle=$(TD)vdjcdca.fle

$(TD)vdjcdul.fle:  vdjcdul.lgc
    $(CC) $(CFLAGS) appl=vdjcdul dotfle=$(TD)vdjcdul.fle

$(TD)ucomed.fle:  ucomed.lgc
    $(CC) $(CFLAGS) appl=ucomed dotfle=$(TD)ucomed.fle
    $(CC) $(CFLAGS) appl=ucomed dotfle=$(FD)ucomed.fle

$(TD)malop.fle:  malop.lgc
    $(CC) $(CFLAGS) appl=malop dotfle=$(TD)malop.fle

$(TD)mk_vpon.fle:  mk_vpon.lgc
    $(CC) $(CFLAGS) appl=mk_vpon dotfle=$(TD)mk_vpon.fle

$(TD)lmk_vpon.fle:  lmk_vpon.lgc
    $(CC) $(CFLAGS) appl=lmk_vpon dotfle=$(TD)lmk_vpon.fle

$(TD)mk_vpons.fle:  mk_vpons.lgc
    $(CC) $(CFLAGS) appl=mk_vpons dotfle=$(TD)mk_vpons.fle

$(TD)vd_sklad.fle:  vd_sklad.lgc
    $(CC) $(CFLAGS) appl=vd_sklad dotfle=$(TD)vd_sklad.fle

$(TD)vd_updtecaj.fle:  vd_updtecaj.lgc
    $(CC) $(CFLAGS) appl=vd_updtecaj dotfle=$(TD)vd_updtecaj.fle

$(TD)aca_acde.fle:  aca_acde.lgc
    $(CC) $(CFLAGS) appl=aca_acde dotfle=$(TD)aca_acde.fle

$(TD)acde.fle:  acde.lgc
    $(CC) $(CFLAGS) appl=acde dotfle=$(TD)acde.fle

$(TD)acde_stan.fle:  acde_stan.lgc
    $(CC) $(CFLAGS) appl=acde_stan dotfle=$(TD)acde_stan.fle

$(TD)vd_ackat.fle:  vd_ackat.lgc
    $(CC) $(CFLAGS) appl=vd_ackat dotfle=$(TD)vd_ackat.fle

$(TD)rnsfaktzb.fle:  rnsfaktzb.lgc
    $(CC) $(CFLAGS) appl=rnsfaktzb dotfle=$(TD)rnsfaktzb.fle

$(TD)fiattp_ins.fle:  fiattp_ins.lgc
    $(CC) $(CFLAGS) appl=fiattp_ins dotfle=$(TD)fiattp_ins.fle

$(TD)fiat_marcia.fle:  fiat_marcia.lgc
    $(CC) $(CFLAGS) appl=fiat_marcia dotfle=$(TD)fiat_marcia.fle

$(TD)inspektori.fle:  inspektori.lgc
    $(CC) $(CFLAGS) appl=inspektori dotfle=$(TD)inspektori.fle

$(TD)gar_dealer.fle:  gar_dealer.lgc
    $(CC) $(CFLAGS) appl=gar_dealer dotfle=$(TD)gar_dealer.fle

$(TD)garservisi.fle:  garservisi.lgc
    $(CC) $(CFLAGS) appl=garservisi dotfle=$(TD)garservisi.fle

$(TD)fiattp_txt.fle:  fiattp_txt.lgc
    $(CC) $(CFLAGS) appl=fiattp_txt dotfle=$(TD)fiattp_txt.fle

$(TD)gar_action.fle:  gar_action.lgc
    $(CC) $(CFLAGS) appl=gar_action dotfle=$(TD)gar_action.fle

$(TD)rns_vehicle.fle:  rns_vehicle.lgc
    $(CC) $(CFLAGS) appl=rns_vehicle dotfle=$(TD)rns_vehicle.fle

$(TD)tip_ppar.fle:  tip_ppar.lgc
    $(CC) $(CFLAGS) appl=tip_ppar dotfle=$(TD)tip_ppar.fle

$(TD)rd_ppar.fle:  rd_ppar.lgc
    $(CC) $(CFLAGS) appl=rd_ppar dotfle=$(TD)rd_ppar.fle

$(TD)rd_cjenvp.fle:  rd_cjenvp.lgc
    $(CC) $(CFLAGS) appl=rd_cjenvp dotfle=$(TD)rd_cjenvp.fle

$(TD)fndorders.fle:  fndorders.lgc
    $(CC) $(CFLAGS) appl=fndorders dotfle=$(TD)fndorders.fle

$(TD)order_conf.fle:  order_conf.lgc
    $(CC) $(CFLAGS) appl=order_conf dotfle=$(TD)order_conf.fle

$(TD)fdopune.fle:  fdopune.lgc
    $(CC) $(CFLAGS) appl=fdopune dotfle=$(TD)fdopune.fle
    $(CC) $(CFLAGS) appl=fdopune dotfle=$(FD)fdopune.fle

$(TD)f01_varexcel.fle:  f01_varexcel.lgc
    $(CC) $(CFLAGS) appl=f01_varexcel dotfle=$(TD)f01_varexcel.fle

$(TD)mkt_jm.fle:  mkt_jm.lgc
    $(CC) $(CFLAGS) appl=mkt_jm dotfle=$(TD)mkt_jm.fle

$(TD)l_mkt_jm.fle:  l_mkt_jm.lgc
    $(CC) $(CFLAGS) appl=l_mkt_jm dotfle=$(TD)l_mkt_jm.fle

$(TD)mkaltskl.fle:  mkaltskl.lgc
    $(CC) $(CFLAGS) appl=mkaltskl dotfle=$(TD)mkaltskl.fle

$(TD)m_trvpmp.fle:  m_trvpmp.lgc
    $(CC) $(CFLAGS) appl=m_trvpmp dotfle=$(TD)m_trvpmp.fle

$(TD)man_katalog.fle:  man_katalog.lgc
    $(CC) $(CFLAGS) appl=man_katalog dotfle=$(TD)man_katalog.fle

$(TD)lm_altskl.fle:  lm_altskl.lgc
    $(CC) $(CFLAGS) appl=lm_altskl dotfle=$(TD)lm_altskl.fle

$(TD)matshknj.fle:  matshknj.lgc
    $(CC) $(CFLAGS) appl=matshknj dotfle=$(TD)matshknj.fle

$(TD)plargo.fle:  plargo.lgc
    $(CC) $(CFLAGS) appl=plargo dotfle=$(TD)plargo.fle

$(TD)plargos.fle:  plargos.lgc
    $(CC) $(CFLAGS) appl=plargos dotfle=$(TD)plargos.fle

$(TD)bc128ab.fle:  bc128ab.lgc
    $(CC) $(CFLAGS) appl=bc128ab dotfle=$(TD)bc128ab.fle

$(TD)mkknjbl.fle:  mkknjbl.lgc
    $(CC) $(CFLAGS) appl=mkknjbl dotfle=$(TD)mkknjbl.fle

$(TD)progs_mat.fle:  progs_mat.lgc
    $(CC) $(CFLAGS) appl=progs_mat dotfle=$(TD)progs_mat.fle

$(TD)mk_knjiz.fle:  mk_knjiz.lgc
    $(CC) $(CFLAGS) appl=mk_knjiz dotfle=$(TD)mk_knjiz.fle

$(TD)prenum_KP.fle:  prenum_KP.lgc
    $(CC) $(CFLAGS) appl=prenum_KP dotfle=$(TD)prenum_KP.fle

$(TD)aca_tempario.fle:  aca_tempario.lgc
    $(CC) $(CFLAGS) appl=aca_tempario dotfle=$(TD)aca_tempario.fle

#########################################################################
#
# Unosi dokumenata i ispisi obrazaca
#
#########################################################################
    
$(TD)mk_ps.fle:  mk_ps.lgc
    $(CC) $(CFLAGS) appl=mk_ps dotfle=$(TD)mk_ps.fle

$(TD)mk_pscje.fle:  mk_pscje.lgc
    $(CC) $(CFLAGS) appl=mk_pscje dotfle=$(TD)mk_pscje.fle

$(TD)lps.fle:  lps.lgc
    $(CC) $(CFLAGS) appl=lps dotfle=$(TD)lps.fle

$(TD)mk_prim.fle:  mk_prim.lgc
    $(CC) $(CFLAGS) appl=mk_prim dotfle=$(TD)mk_prim.fle
    
$(TD)mk_prims.fle:  mk_prims.lgc
    $(CC) $(CFLAGS) appl=mk_prims dotfle=$(TD)mk_prims.fle

$(TD)lprimke.fle:  lprimke.lgc
    $(CC) $(CFLAGS) appl=lprimke dotfle=$(TD)lprimke.fle
    
$(TD)lprimke1.fle:  lprimke1.lgc
    $(CC) $(CFLAGS) appl=lprimke1 dotfle=$(TD)lprimke1.fle
    
$(TD)lprimke2.fle:  lprimke2.lgc
    $(CC) $(CFLAGS) appl=lprimke2 dotfle=$(TD)lprimke2.fle
    
$(TD)lprimke3.fle:  lprimke3.lgc
    $(CC) $(CFLAGS) appl=lprimke3 dotfle=$(TD)lprimke3.fle
    
$(TD)lprimke4.fle:  lprimke4.lgc
    $(CC) $(CFLAGS) appl=lprimke4 dotfle=$(TD)lprimke4.fle
    
$(TD)mk_kumul.fle:  mk_kumul.lgc
    $(CC) $(CFLAGS) appl=mk_kumul dotfle=$(TD)mk_kumul.fle
    
$(TD)mk_kalk.fle:  mk_kalk.lgc
    $(CC) $(CFLAGS) appl=mk_kalk dotfle=$(TD)mk_kalk.fle
    
$(TD)mk_kalks.fle:  mk_kalks.lgc
    $(CC) $(CFLAGS) appl=mk_kalks dotfle=$(TD)mk_kalks.fle
    
$(TD)lkalkul.fle:  lkalkul.lgc
    $(CC) $(CFLAGS) appl=lkalkul dotfle=$(TD)lkalkul.fle
    
$(TD)lkalkul_ac.fle:  lkalkul_ac.lgc
    $(CC) $(CFLAGS) appl=lkalkul_ac dotfle=$(TD)lkalkul_ac.fle
    
$(TD)lkalkul_AMEH.fle:  lkalkul_AMEH.lgc
    $(CC) $(CFLAGS) appl=lkalkul_AMEH dotfle=$(TD)lkalkul_AMEH.fle
    
$(TD)lrnsvlas.fle:  lrnsvlas.lgc
    $(CC) $(CFLAGS) appl=lrnsvlas dotfle=$(TD)lrnsvlas.fle

$(TD)mk_izd.fle:  mk_izd.lgc
    $(CC) $(CFLAGS) appl=mk_izd dotfle=$(TD)mk_izd.fle

$(TD)mk_izds.fle:  mk_izds.lgc
    $(CC) $(CFLAGS) appl=mk_izds dotfle=$(TD)mk_izds.fle
    
$(TD)lizdat.fle:  lizdat.lgc
    $(CC) $(CFLAGS) appl=lizdat dotfle=$(TD)lizdat.fle
    
$(TD)lizdat_dt.fle:  lizdat_dt.lgc
    $(CC) $(CFLAGS) appl=lizdat_dt dotfle=$(TD)lizdat_dt.fle

$(TD)lizdat_car.fle:  lizdat_car.lgc
    $(CC) $(CFLAGS) appl=lizdat_car dotfle=$(TD)lizdat_car.fle

$(TD)mk_otp.fle:  mk_otp.lgc
    $(CC) $(CFLAGS) appl=mk_otp dotfle=$(TD)mk_otp.fle

$(TD)ml_otp.fle:  ml_otp.lgc
    $(CC) $(CFLAGS) appl=ml_otp dotfle=$(TD)ml_otp.fle

$(TD)mk_otps.fle:  mk_otps.lgc
    $(CC) $(CFLAGS) appl=mk_otps dotfle=$(TD)mk_otps.fle
    
$(TD)lotp.fle:  lotp.lgc
    $(CC) $(CFLAGS) appl=lotp dotfle=$(TD)lotp.fle
    
$(TD)lotp_din.fle:  lotp_din.lgc
    $(CC) $(CFLAGS) appl=lotp_din dotfle=$(TD)lotp_din.fle
    
$(TD)lprim_din.fle:  lprim_din.lgc
    $(CC) $(CFLAGS) appl=lprim_din dotfle=$(TD)lprim_din.fle
    
$(TD)lizd_din.fle:  lizd_din.lgc
    $(CC) $(CFLAGS) appl=lizd_din dotfle=$(TD)lizd_din.fle
    
$(TD)lnal_otp.fle:  lnal_otp.lgc
    $(CC) $(CFLAGS) appl=lnal_otp dotfle=$(TD)lnal_otp.fle
    
$(TD)lnal_otp_ml.fle:  lnal_otp_ml.lgc
    $(CC) $(CFLAGS) appl=lnal_otp_ml dotfle=$(TD)lnal_otp_ml.fle
    
$(TD)vd_akcija.fle:  vd_akcija.lgc
    $(CC) $(CFLAGS) appl=vd_akcija dotfle=$(TD)vd_akcija.fle
    
$(TD)vd_leasing.fle:  vd_leasing.lgc
    $(CC) $(CFLAGS) appl=vd_leasing dotfle=$(TD)vd_leasing.fle
    
$(TD)vd_leas_file.fle:  vd_leas_file.lgc
    $(CC) $(CFLAGS) appl=vd_leas_file dotfle=$(TD)vd_leas_file.fle
    
$(TD)lvoz_pon_dev.fle:  lvoz_pon_dev.lgc
    $(CC) $(CFLAGS) appl=lvoz_pon_dev dotfle=$(TD)lvoz_pon_dev.fle
    
$(TD)vd_zap_akcija.fle:  vd_zap_akcija.lgc
    $(CC) $(CFLAGS) appl=vd_zap_akcija dotfle=$(TD)vd_zap_akcija.fle
    
$(TD)mk_msk.fle:  mk_msk.lgc
    $(CC) $(CFLAGS) appl=mk_msk dotfle=$(TD)mk_msk.fle

$(TD)mk_msks.fle:  mk_msks.lgc
    $(CC) $(CFLAGS) appl=mk_msks dotfle=$(TD)mk_msks.fle
    
$(TD)lmsk.fle:  lmsk.lgc
    $(CC) $(CFLAGS) appl=lmsk dotfle=$(TD)lmsk.fle
    
$(TD)mk_neklk.fle:  mk_neklk.lgc
    $(CC) $(CFLAGS) appl=mk_neklk dotfle=$(TD)mk_neklk.fle
    
$(TD)lneobrac.fle:  lneobrac.lgc
    $(CC) $(CFLAGS) appl=lneobrac dotfle=$(TD)lneobrac.fle

$(TD)mk_obr.fle:  mk_obr.lgc
    $(CC) $(CFLAGS) appl=mk_obr dotfle=$(TD)mk_obr.fle
    
$(TD)mk_poppr.fle:  mk_poppr.lgc
    $(CC) $(CFLAGS) appl=mk_poppr dotfle=$(TD)mk_poppr.fle
    
$(TD)mk_poppr_hd.fle:  mk_poppr_hd.lgc
    $(CC) $(CFLAGS) appl=mk_poppr_hd dotfle=$(TD)mk_poppr_hd.fle
    
$(TD)lotprn.fle:  lotprn.lgc
    $(CC) $(CFLAGS) appl=lotprn dotfle=$(TD)lotprn.fle
    
$(TD)lotprn_r1.fle:  lotprn_r1.lgc
    $(CC) $(CFLAGS) appl=lotprn_r1 dotfle=$(TD)lotprn_r1.fle

$(TD)lotp_hpa.fle:  lotp_hpa.lgc
    $(CC) $(CFLAGS) appl=lotp_hpa dotfle=$(TD)lotp_hpa.fle

$(TD)mk_kotp.fle:  mk_kotp.lgc
    $(CC) $(CFLAGS) appl=mk_kotp dotfle=$(TD)mk_kotp.fle
    
$(TD)mk_kotps.fle:  mk_kotps.lgc
    $(CC) $(CFLAGS) appl=mk_kotps dotfle=$(TD)mk_kotps.fle
    
$(TD)rmsk.fle:  rmsk.lgc
    $(CC) $(CFLAGS) appl=rmsk dotfle=$(TD)rmsk.fle
    
$(TD)mk_mskul.fle:  mk_mskul.lgc
    $(CC) $(CFLAGS) appl=mk_mskul dotfle=$(TD)mk_mskul.fle
    
$(TD)rotprn.fle:  rotprn.lgc
    $(CC) $(CFLAGS) appl=rotprn dotfle=$(TD)rotprn.fle
    
$(TD)lotprnps.fle:  lotprnps.lgc
    $(CC) $(CFLAGS) appl=lotprnps dotfle=$(TD)lotprnps.fle
    
$(TD)mkupdcje.fle:  mkupdcje.lgc
    $(CC) $(CFLAGS) appl=mkupdcje dotfle=$(TD)mkupdcje.fle
    
$(TD)mk_treb.fle:  mk_treb.lgc
    $(CC) $(CFLAGS) appl=mk_treb dotfle=$(TD)mk_treb.fle
    
$(TD)mk_trebs.fle:  mk_trebs.lgc
    $(CC) $(CFLAGS) appl=mk_trebs dotfle=$(TD)mk_trebs.fle
    
$(TD)lmk_treb.fle:  lmk_treb.lgc
    $(CC) $(CFLAGS) appl=lmk_treb dotfle=$(TD)lmk_treb.fle
    
$(TD)rmk_treb.fle:  rmk_treb.lgc
    $(CC) $(CFLAGS) appl=rmk_treb dotfle=$(TD)rmk_treb.fle
    
$(TD)rizdat.fle:  rizdat.lgc
    $(CC) $(CFLAGS) appl=rizdat dotfle=$(TD)rizdat.fle
    
$(TD)rotp.fle:  rotp.lgc
    $(CC) $(CFLAGS) appl=rotp dotfle=$(TD)rotp.fle
    
$(TD)rprimke.fle:  rprimke.lgc
    $(CC) $(CFLAGS) appl=rprimke dotfle=$(TD)rprimke.fle
    
$(TD)l_narkzb.fle:  l_narkzb.lgc
    $(CC) $(CFLAGS) appl=l_narkzb dotfle=$(TD)l_narkzb.fle
    
$(TD)mk_nark.fle:  mk_nark.lgc
    $(CC) $(CFLAGS) appl=mk_nark dotfle=$(TD)mk_nark.fle
    
$(TD)mk_narks.fle:  mk_narks.lgc
    $(CC) $(CFLAGS) appl=mk_narks dotfle=$(TD)mk_narks.fle
    
$(TD)mk_nard.fle:  mk_nard.lgc
    $(CC) $(CFLAGS) appl=mk_nard dotfle=$(TD)mk_nard.fle
    
$(TD)mk_nards.fle:  mk_nards.lgc
    $(CC) $(CFLAGS) appl=mk_nards dotfle=$(TD)mk_nards.fle
    
$(TD)mkgetkat.fle:  mkgetkat.lgc
    $(CC) $(CFLAGS) appl=mkgetkat dotfle=$(TD)mkgetkat.fle
    
$(TD)mkgetsta.fle:  mkgetsta.lgc
    $(CC) $(CFLAGS) appl=mkgetsta dotfle=$(TD)mkgetsta.fle
    
$(TD)posprim.fle:  posprim.lgc
    $(CC) $(CFLAGS) appl=posprim dotfle=$(TD)posprim.fle
    
$(TD)vrs_oper.fle:  vrs_oper.lgc
    $(CC) $(CFLAGS) appl=vrs_oper dotfle=$(TD)vrs_oper.fle
    
$(TD)rns_oper.fle:  rns_oper.lgc
    $(CC) $(CFLAGS) appl=rns_oper dotfle=$(TD)rns_oper.fle
    
$(TD)rns_pfakt.fle:  rns_pfakt.lgc
    $(CC) $(CFLAGS) appl=rns_pfakt dotfle=$(TD)rns_pfakt.fle
    
$(TD)rns_2fakt.fle:  rns_2fakt.lgc
    $(CC) $(CFLAGS) appl=rns_2fakt dotfle=$(TD)rns_2fakt.fle
    
$(TD)rns_prij.fle:  rns_prij.lgc
    $(CC) $(CFLAGS) appl=rns_prij dotfle=$(TD)rns_prij.fle
    
$(TD)ml_prij.fle:  ml_prij.lgc
    $(CC) $(CFLAGS) appl=ml_prij dotfle=$(TD)ml_prij.fle
    
$(TD)rns_vlas.fle:  rns_vlas.lgc
    $(CC) $(CFLAGS) appl=rns_vlas dotfle=$(TD)rns_vlas.fle
    
$(TD)rns_voz.fle:  rns_voz.lgc
    $(CC) $(CFLAGS) appl=rns_voz dotfle=$(TD)rns_voz.fle

$(TD)rns_zabran.fle:  rns_zabran.lgc
    $(CC) $(CFLAGS) appl=rns_zabran dotfle=$(TD)rns_zabran.fle

$(TD)rns_kola.fle:  rns_kola.lgc
    $(CC) $(CFLAGS) appl=rns_kola dotfle=$(TD)rns_kola.fle

$(TD)rns_servisi.fle:  rns_servisi.lgc
    $(CC) $(CFLAGS) appl=rns_servisi dotfle=$(TD)rns_servisi.fle

$(TD)rns_radnik.fle:  rns_radnik.lgc
    $(CC) $(CFLAGS) appl=rns_radnik dotfle=$(TD)rns_radnik.fle

$(TD)lmpnaldek.fle:  lmpnaldek.lgc
    $(CC) $(CFLAGS) appl=lmpnaldek dotfle=$(TD)lmpnaldek.fle

$(TD)lvpnaldek.fle:  lvpnaldek.lgc
    $(CC) $(CFLAGS) appl=lvpnaldek dotfle=$(TD)lvpnaldek.fle

$(TD)reparirani.fle:  reparirani.lgc
    $(CC) $(CFLAGS) appl=reparirani dotfle=$(TD)reparirani.fle

$(TD)mpallnal.fle:  mpallnal.lgc
    $(CC) $(CFLAGS) appl=mpallnal dotfle=$(TD)mpallnal.fle

$(TD)vpallnal.fle:  vpallnal.lgc
    $(CC) $(CFLAGS) appl=vpallnal dotfle=$(TD)vpallnal.fle

$(TD)lrn_prij.fle:  lrn_prij.lgc
    $(CC) $(CFLAGS) appl=lrn_prij dotfle=$(TD)lrn_prij.fle

$(TD)lrn_prij_ml.fle:  lrn_prij_ml.lgc
    $(CC) $(CFLAGS) appl=lrn_prij_ml dotfle=$(TD)lrn_prij_ml.fle

$(TD)lrn_prij_ac.fle:  lrn_prij_ac.lgc
    $(CC) $(CFLAGS) appl=lrn_prij_ac dotfle=$(TD)lrn_prij_ac.fle

$(TD)lrnkolar.fle:  lrnkolar.lgc
    $(CC) $(CFLAGS) appl=lrnkolar dotfle=$(TD)lrnkolar.fle

$(TD)lrnkolam.fle:  lrnkolam.lgc
    $(CC) $(CFLAGS) appl=lrnkolam dotfle=$(TD)lrnkolam.fle

$(TD)mkvrkpop.fle:  mkvrkpop.lgc
    $(CC) $(CFLAGS) appl=mkvrkpop dotfle=$(TD)mkvrkpop.fle

$(TD)mk_kpop.fle:  mk_kpop.lgc
    $(CC) $(CFLAGS) appl=mk_kpop dotfle=$(TD)mk_kpop.fle

$(TD)otp_kpop.fle:  otp_kpop.lgc
    $(CC) $(CFLAGS) appl=otp_kpop dotfle=$(TD)otp_kpop.fle

$(TD)rns_obr.fle:  rns_obr.lgc
    $(CC) $(CFLAGS) appl=rns_obr dotfle=$(TD)rns_obr.fle

$(TD)rns_grupe.fle:  rns_grupe.lgc
    $(CC) $(CFLAGS) appl=rns_grupe dotfle=$(TD)rns_grupe.fle

$(TD)rns_pgrupe.fle:  rns_pgrupe.lgc
    $(CC) $(CFLAGS) appl=rns_pgrupe dotfle=$(TD)rns_pgrupe.fle

$(TD)lknj_pop.fle:  lknj_pop.lgc
    $(CC) $(CFLAGS) appl=lknj_pop dotfle=$(TD)lknj_pop.fle

$(TD)mk_mprn.fle:  mk_mprn.lgc
    $(CC) $(CFLAGS) appl=mk_mprn dotfle=$(TD)mk_mprn.fle

$(TD)mk_kalkps.fle:  mk_kalkps.lgc
    $(CC) $(CFLAGS) appl=mk_kalkps dotfle=$(TD)mk_kalkps.fle

$(TD)mkprmpvp.fle:  mkprmpvp.lgc
    $(CC) $(CFLAGS) appl=mkprmpvp dotfle=$(TD)mkprmpvp.fle

$(TD)mkprvpmp.fle:  mkprvpmp.lgc
    $(CC) $(CFLAGS) appl=mkprvpmp dotfle=$(TD)mkprvpmp.fle

$(TD)mkprmpmp.fle:  mkprmpmp.lgc
    $(CC) $(CFLAGS) appl=mkprmpmp dotfle=$(TD)mkprmpmp.fle

$(TD)mkstmpmp.fle:  mkstmpmp.lgc
    $(CC) $(CFLAGS) appl=mkstmpmp dotfle=$(TD)mkstmpmp.fle

$(TD)lmskmpvp.fle:  lmskmpvp.lgc
    $(CC) $(CFLAGS) appl=lmskmpvp dotfle=$(TD)lmskmpvp.fle

$(TD)lmskmpmp.fle:  lmskmpmp.lgc
    $(CC) $(CFLAGS) appl=lmskmpmp dotfle=$(TD)lmskmpmp.fle

$(TD)mat_uplate.fle:  mat_uplate.lgc
    $(CC) $(CFLAGS) appl=mat_uplate dotfle=$(TD)mat_uplate.fle

$(TD)rtermo_nal.fle:  rtermo_nal.lgc
    $(CC) $(CFLAGS) appl=rtermo_nal dotfle=$(TD)rtermo_nal.fle

$(TD)mkstmpvp.fle:  mkstmpvp.lgc
    $(CC) $(CFLAGS) appl=mkstmpvp dotfle=$(TD)mkstmpvp.fle

$(TD)mkstvpmp.fle:  mkstvpmp.lgc
    $(CC) $(CFLAGS) appl=mkstvpmp dotfle=$(TD)mkstvpmp.fle

$(TD)popust_mp.fle:  popust_mp.lgc
    $(CC) $(CFLAGS) appl=popust_mp dotfle=$(TD)popust_mp.fle

$(TD)mk_mpupl.fle:  mk_mpupl.lgc
    $(CC) $(CFLAGS) appl=mk_mpupl dotfle=$(TD)mk_mpupl.fle

$(TD)rns_mpupl.fle:  rns_mpupl.lgc
    $(CC) $(CFLAGS) appl=rns_mpupl dotfle=$(TD)rns_mpupl.fle

$(TD)mprnpreg.fle:  mprnpreg.lgc
    $(CC) $(CFLAGS) appl=mprnpreg dotfle=$(TD)mprnpreg.fle

$(TD)lmkdntot.fle:  lmkdntot.lgc
    $(CC) $(CFLAGS) appl=lmkdntot dotfle=$(TD)lmkdntot.fle

$(TD)lnk_rast.fle:  lnk_rast.lgc
    $(CC) $(CFLAGS) appl=lnk_rast dotfle=$(TD)lnk_rast.fle

$(TD)lpr_rnal.fle:  lpr_rnal.lgc
    $(CC) $(CFLAGS) appl=lpr_rnal dotfle=$(TD)lpr_rnal.fle

$(TD)vd_vozila.fle:  vd_vozila.lgc
    $(CC) $(CFLAGS) appl=vd_vozila dotfle=$(TD)vd_vozila.fle

$(TD)vd_proc.fle:  vd_proc.lgc
    $(CC) $(CFLAGS) appl=vd_proc dotfle=$(TD)vd_proc.fle

$(TD)vd_kateg.fle:  vd_kateg.lgc
    $(CC) $(CFLAGS) appl=vd_kateg dotfle=$(TD)vd_kateg.fle

$(TD)vd_izborpod.fle:  vd_izborpod.lgc
    $(CC) $(CFLAGS) appl=vd_izborpod dotfle=$(TD)vd_izborpod.fle

$(TD)lvd_proc.fle:  lvd_proc.lgc
    $(CC) $(CFLAGS) appl=lvd_proc dotfle=$(TD)lvd_proc.fle

$(TD)lvd_otkup.fle:  lvd_otkup.lgc
    $(CC) $(CFLAGS) appl=lvd_otkup dotfle=$(TD)lvd_otkup.fle

$(TD)lvd_otkzap.fle:  lvd_otkzap.lgc
    $(CC) $(CFLAGS) appl=lvd_otkzap dotfle=$(TD)lvd_otkzap.fle

$(TD)lvd_finlag.fle:  lvd_finlag.lgc
    $(CC) $(CFLAGS) appl=lvd_finlag dotfle=$(TD)lvd_finlag.fle

$(TD)vd_fndfakt.fle:  vd_fndfakt.lgc
    $(CC) $(CFLAGS) appl=vd_fndfakt dotfle=$(TD)vd_fndfakt.fle

$(TD)vd_fndprim.fle:  vd_fndprim.lgc
    $(CC) $(CFLAGS) appl=vd_fndprim dotfle=$(TD)vd_fndprim.fle

$(TD)lvoz_otprac.fle:  lvoz_otprac.lgc
    $(CC) $(CFLAGS) appl=lvoz_otprac dotfle=$(TD)lvoz_otprac.fle

$(TD)l_otpracfo.fle:  l_otpracfo.lgc
    $(CC) $(CFLAGS) appl=l_otpracfo dotfle=$(TD)l_otpracfo.fle

$(TD)lvoz_otpracAM.fle:  lvoz_otpracAM.lgc
    $(CC) $(CFLAGS) appl=lvoz_otpracAM dotfle=$(TD)lvoz_otpracAM.fle

$(TD)lvoz_racHONDA.fle:  lvoz_racHONDA.lgc
    $(CC) $(CFLAGS) appl=lvoz_racHONDA dotfle=$(TD)lvoz_racHONDA.fle

$(TD)l_racHONDAfo.fle:  l_racHONDAfo.lgc
    $(CC) $(CFLAGS) appl=l_racHONDAfo dotfle=$(TD)l_racHONDAfo.fle

$(TD)lvoz_otpHONDA.fle:  lvoz_otpHONDA.lgc
    $(CC) $(CFLAGS) appl=lvoz_otpHONDA dotfle=$(TD)lvoz_otpHONDA.fle

$(TD)l_otpHONDAfo.fle:  l_otpHONDAfo.lgc
    $(CC) $(CFLAGS) appl=l_otpHONDAfo dotfle=$(TD)l_otpHONDAfo.fle

$(TD)lvd_infhonda.fle:  lvd_infhonda.lgc
    $(CC) $(CFLAGS) appl=lvd_infhonda dotfle=$(TD)lvd_infhonda.fle

$(TD)lvd_infhonda1.fle:  lvd_infhonda1.lgc
    $(CC) $(CFLAGS) appl=lvd_infhonda1 dotfle=$(TD)lvd_infhonda1.fle

$(TD)lvoz_ponpict.fle:  lvoz_ponpict.lgc
    $(CC) $(CFLAGS) appl=lvoz_ponpict dotfle=$(TD)lvoz_ponpict.fle

$(TD)lvoz_ponpict2.fle:  lvoz_ponpict2.lgc
    $(CC) $(CFLAGS) appl=lvoz_ponpict2 dotfle=$(TD)lvoz_ponpict2.fle

$(TD)vd_infponude.fle:  vd_infponude.lgc
    $(CC) $(CFLAGS) appl=vd_infponude dotfle=$(TD)vd_infponude.fle

$(TD)lvd_infpon.fle:  lvd_infpon.lgc
    $(CC) $(CFLAGS) appl=lvd_infpon dotfle=$(TD)lvd_infpon.fle

$(TD)lvd_infponp1.fle:  lvd_infponp1.lgc
    $(CC) $(CFLAGS) appl=lvd_infponp1 dotfle=$(TD)lvd_infponp1.fle

$(TD)lvd_infponp2.fle:  lvd_infponp2.lgc
    $(CC) $(CFLAGS) appl=lvd_infponp2 dotfle=$(TD)lvd_infponp2.fle

$(TD)rns_najava.fle:  rns_najava.lgc
    $(CC) $(CFLAGS) appl=rns_najava dotfle=$(TD)rns_najava.fle

$(TD)rn_kontakti.fle:  rn_kontakti.lgc
    $(CC) $(CFLAGS) appl=rn_kontakti dotfle=$(TD)rn_kontakti.fle

$(TD)lrns_kontakt.fle:  lrns_kontakt.lgc
    $(CC) $(CFLAGS) appl=lrns_kontakt dotfle=$(TD)lrns_kontakt.fle

$(TD)lvoz_racSPC.fle:  lvoz_racSPC.lgc
    $(CC) $(CFLAGS) appl=lvoz_racSPC dotfle=$(TD)lvoz_racSPC.fle

$(TD)lvozracSPC1.fle:  lvozracSPC1.lgc
    $(CC) $(CFLAGS) appl=lvozracSPC1 dotfle=$(TD)lvozracSPC1.fle

$(TD)vd_dodop.fle:  vd_dodop.lgc
    $(CC) $(CFLAGS) appl=vd_dodop dotfle=$(TD)vd_dodop.fle

$(TD)vd_vrsleas.fle:  vd_vrsleas.lgc
    $(CC) $(CFLAGS) appl=vd_vrsleas dotfle=$(TD)vd_vrsleas.fle

$(TD)lvoz_primka.fle:  lvoz_primka.lgc
    $(CC) $(CFLAGS) appl=lvoz_primka dotfle=$(TD)lvoz_primka.fle

$(TD)lvoz_izdat.fle:  lvoz_izdat.lgc
    $(CC) $(CFLAGS) appl=lvoz_izdat dotfle=$(TD)lvoz_izdat.fle

$(TD)rns_marke.fle:  rns_marke.lgc
    $(CC) $(CFLAGS) appl=rns_marke dotfle=$(TD)rns_marke.fle

$(TD)voz_zaliha.fle:  voz_zaliha.lgc
    $(CC) $(CFLAGS) appl=voz_zaliha dotfle=$(TD)voz_zaliha.fle

$(TD)vozilaobr.fle:  vozilaobr.lgc
    $(CC) $(CFLAGS) appl=vozilaobr dotfle=$(TD)vozilaobr.fle

$(TD)vdponobr.fle:  vdponobr.lgc
    $(CC) $(CFLAGS) appl=vdponobr dotfle=$(TD)vdponobr.fle

$(TD)vd_kartvoz.fle:  vd_kartvoz.lgc
    $(CC) $(CFLAGS) appl=vd_kartvoz dotfle=$(TD)vd_kartvoz.fle

$(TD)vd_popis.fle:  vd_popis.lgc
    $(CC) $(CFLAGS) appl=vd_popis dotfle=$(TD)vd_popis.fle

$(TD)vd_pop_dani.fle:  vd_pop_dani.lgc
    $(CC) $(CFLAGS) appl=vd_pop_dani dotfle=$(TD)vd_pop_dani.fle

$(TD)vd_vozAC.fle:  vd_vozAC.lgc
    $(CC) $(CFLAGS) appl=vd_vozAC dotfle=$(TD)vd_vozAC.fle

$(TD)vd_vozAC_nf.fle:  vd_vozAC_nf.lgc
    $(CC) $(CFLAGS) appl=vd_vozAC_nf dotfle=$(TD)vd_vozAC_nf.fle

$(TD)vd_uplate.fle:  vd_uplate.lgc
    $(CC) $(CFLAGS) appl=vd_uplate dotfle=$(TD)vd_uplate.fle

$(TD)lvd_uplate.fle:  lvd_uplate.lgc
    $(CC) $(CFLAGS) appl=lvd_uplate dotfle=$(TD)lvd_uplate.fle

$(TD)vd_dodopAC.fle:  vd_dodopAC.lgc
    $(CC) $(CFLAGS) appl=vd_dodopAC dotfle=$(TD)vd_dodopAC.fle

$(TD)mpponzag.fle:  mpponzag.lgc
    $(CC) $(CFLAGS) appl=mpponzag dotfle=$(TD)mpponzag.fle

$(TD)mpponstav.fle:  mpponstav.lgc
    $(CC) $(CFLAGS) appl=mpponstav dotfle=$(TD)mpponstav.fle

$(TD)lmppon.fle:  lmppon.lgc
    $(CC) $(CFLAGS) appl=lmppon dotfle=$(TD)lmppon.fle

$(TD)rmpstar.fle:  rmpstar.lgc
    $(CC) $(CFLAGS) appl=rmpstar dotfle=$(TD)rmpstar.fle

$(TD)rmpr1.fle:  rmpr1.lgc
    $(CC) $(CFLAGS) appl=rmpr1 dotfle=$(TD)rmpr1.fle

$(TD)lvd_zap.fle:  lvd_zap.lgc
    $(CC) $(CFLAGS) appl=lvd_zap dotfle=$(TD)lvd_zap.fle

$(TD)lvd_zahtjev.fle:  lvd_zahtjev.lgc
    $(CC) $(CFLAGS) appl=lvd_zahtjev dotfle=$(TD)lvd_zahtjev.fle

$(TD)vd_kartdor.fle:  vd_kartdor.lgc
    $(CC) $(CFLAGS) appl=vd_kartdor dotfle=$(TD)vd_kartdor.fle

$(TD)lvd_kartdor.fle:  lvd_kartdor.lgc
    $(CC) $(CFLAGS) appl=lvd_kartdor dotfle=$(TD)lvd_kartdor.fle

$(TD)lvd_zapAM.fle:  lvd_zapAM.lgc
    $(CC) $(CFLAGS) appl=lvd_zapAM dotfle=$(TD)lvd_zapAM.fle

$(TD)rnsoperdi.fle:  rnsoperdi.lgc
    $(CC) $(CFLAGS) appl=rnsoperdi dotfle=$(TD)rnsoperdi.fle

$(TD)lrnsplaner_nr.fle:  lrnsplaner_nr.lgc
    $(CC) $(CFLAGS) appl=lrnsplaner_nr dotfle=$(TD)lrnsplaner_nr.fle

$(TD)rns_planer.fle:  rns_planer.lgc
    $(CC) $(CFLAGS) appl=rns_planer dotfle=$(TD)rns_planer.fle

$(TD)rns_planst.fle:  rns_planst.lgc
    $(CC) $(CFLAGS) appl=rns_planst dotfle=$(TD)rns_planst.fle

$(TD)rn_tips.fle:  rn_tips.lgc
    $(CC) $(CFLAGS) appl=rn_tips dotfle=$(TD)rn_tips.fle

$(TD)rns_neactpgr.fle:  rns_neactpgr.lgc
    $(CC) $(CFLAGS) appl=rns_neactpgr dotfle=$(TD)rns_neactpgr.fle

$(TD)lpprodkup.fle:  lpprodkup.lgc
    $(CC) $(CFLAGS) appl=lpprodkup dotfle=$(TD)lpprodkup.fle

$(TD)lprim_fiat.fle:  lprim_fiat.lgc
    $(CC) $(CFLAGS) appl=lprim_fiat dotfle=$(TD)lprim_fiat.fle

$(TD)lmk_rekzs.fle:  lmk_rekzs.lgc
    $(CC) $(CFLAGS) appl=lmk_rekzs dotfle=$(TD)lmk_rekzs.fle

$(TD)rdrazlike.fle:  rdrazlike.lgc
    $(CC) $(CFLAGS) appl=rdrazlike dotfle=$(TD)rdrazlike.fle

$(TD)l_mkaddpr.fle:  l_mkaddpr.lgc
    $(CC) $(CFLAGS) appl=l_mkaddpr dotfle=$(TD)l_mkaddpr.fle

$(TD)rns_izborpod.fle:  rns_izborpod.lgc
    $(CC) $(CFLAGS) appl=rns_izborpod dotfle=$(TD)rns_izborpod.fle

$(TD)lrd_primke.fle:  lrd_primke.lgc
    $(CC) $(CFLAGS) appl=lrd_primke dotfle=$(TD)lrd_primke.fle

$(TD)lmsk_AMEH.fle:  lmsk_AMEH.lgc
    $(CC) $(CFLAGS) appl=lmsk_AMEH dotfle=$(TD)lmsk_AMEH.fle

$(TD)mk2_izl.fle:  mk2_izl.lgc
    $(CC) $(CFLAGS) appl=mk2_izl dotfle=$(TD)mk2_izl.fle

$(TD)mk2_ul.fle:  mk2_ul.lgc
    $(CC) $(CFLAGS) appl=mk2_ul dotfle=$(TD)mk2_ul.fle

$(TD)mkst2_izl.fle:  mkst2_izl.lgc
    $(CC) $(CFLAGS) appl=mkst2_izl dotfle=$(TD)mkst2_izl.fle

$(TD)vd_lvozfk.fle:  vd_lvozfk.lgc
    $(CC) $(CFLAGS) appl=vd_lvozfk dotfle=$(TD)vd_lvozfk.fle

$(TD)mkmpobr.fle:  mkmpobr.lgc
    $(CC) $(CFLAGS) appl=mkmpobr dotfle=$(TD)mkmpobr.fle

$(TD)rnsrac.fle:  rnsrac.lgc
    $(CC) $(CFLAGS) appl=rnsrac dotfle=$(TD)rnsrac.fle

$(TD)rns_fakt.fle:  rns_fakt.lgc
    $(CC) $(CFLAGS) appl=rns_fakt dotfle=$(TD)rns_fakt.fle

$(TD)rns_mfakt.fle:  rns_mfakt.lgc
    $(CC) $(CFLAGS) appl=rns_mfakt dotfle=$(TD)rns_mfakt.fle

$(TD)vd_dnev.fle:  vd_dnev.lgc
    $(CC) $(CFLAGS) appl=vd_dnev dotfle=$(TD)vd_dnev.fle

$(TD)gar_zapis.fle:  gar_zapis.lgc
    $(CC) $(CFLAGS) appl=gar_zapis dotfle=$(TD)gar_zapis.fle

$(TD)vehicleinact.fle:  vehicleinact.lgc
    $(CC) $(CFLAGS) appl=vehicleinact dotfle=$(TD)vehicleinact.fle

$(TD)multifak.fle:  multifak.lgc
    $(CC) $(CFLAGS) appl=multifak dotfle=$(TD)multifak.fle

$(TD)lrnal_IIU.fle:  lrnal_IIU.lgc
    $(CC) $(CFLAGS) appl=lrnal_IIU dotfle=$(TD)lrnal_IIU.fle

$(TD)addmpprim.fle:  addmpprim.lgc
    $(CC) $(CFLAGS) appl=addmpprim dotfle=$(TD)addmpprim.fle

$(TD)addprimps.fle:  addprimps.lgc
    $(CC) $(CFLAGS) appl=addprimps dotfle=$(TD)addprimps.fle

$(TD)rns_operat.fle:  rns_operat.lgc
    $(CC) $(CFLAGS) appl=rns_operat dotfle=$(TD)rns_operat.fle

$(TD)intfakt.fle:  intfakt.lgc
    $(CC) $(CFLAGS) appl=intfakt dotfle=$(TD)intfakt.fle

$(TD)mk_rekz.fle:  mk_rekz.lgc
    $(CC) $(CFLAGS) appl=mk_rekz dotfle=$(TD)mk_rekz.fle

$(TD)mk_rekzs.fle:  mk_rekzs.lgc
    $(CC) $(CFLAGS) appl=mk_rekzs dotfle=$(TD)mk_rekzs.fle

#########################################################################
#
# Izvjesca i inventure
#
#########################################################################
    
$(TD)invisp.fle:  invisp.lgc
    $(CC) $(CFLAGS) appl=invisp dotfle=$(TD)invisp.fle
    
$(TD)mk_viman.fle:  mk_viman.lgc
    $(CC) $(CFLAGS) appl=mk_viman dotfle=$(TD)mk_viman.fle

$(TD)mk_arhin.fle:  mk_arhin.lgc
    $(CC) $(CFLAGS) appl=mk_arhin dotfle=$(TD)mk_arhin.fle
    
$(TD)mk_inisp.fle:  mk_inisp.lgc
    $(CC) $(CFLAGS) appl=mk_inisp dotfle=$(TD)mk_inisp.fle

$(TD)mk_inv.fle:  mk_inv.lgc
    $(CC) $(CFLAGS) appl=mk_inv dotfle=$(TD)mk_inv.fle

$(TD)mkinvadd.fle:  mkinvadd.lgc
    $(CC) $(CFLAGS) appl=mkinvadd dotfle=$(TD)mkinvadd.fle

$(TD)mk_obinv.fle:  mk_obinv.lgc
    $(CC) $(CFLAGS) appl=mk_obinv dotfle=$(TD)mk_obinv.fle

$(TD)mk_kart.fle:  mk_kart.lgc
    $(CC) $(CFLAGS) appl=mk_kart dotfle=$(TD)mk_kart.fle

$(TD)lkart.fle:  lkart.lgc
    $(CC) $(CFLAGS) appl=lkart dotfle=$(TD)lkart.fle
    
$(TD)mk_stay.fle:  mk_stay.lgc
    $(CC) $(CFLAGS) appl=mk_stay dotfle=$(TD)mk_stay.fle

$(TD)lstanje.fle:  lstanje.lgc
    $(CC) $(CFLAGS) appl=lstanje dotfle=$(TD)lstanje.fle

$(TD)mk_dnev.fle:  mk_dnev.lgc
    $(CC) $(CFLAGS) appl=mk_dnev dotfle=$(TD)mk_dnev.fle

$(TD)ldnevnik.fle:  ldnevnik.lgc
    $(CC) $(CFLAGS) appl=ldnevnik dotfle=$(TD)ldnevnik.fle

$(TD)mjtr.fle:  mjtr.lgc
    $(CC) $(CFLAGS) appl=mjtr dotfle=$(TD)mjtr.fle
    
$(TD)ispizd.fle:  ispizd.lgc
    $(CC) $(CFLAGS) appl=ispizd dotfle=$(TD)ispizd.fle
    
$(TD)mk_rekap.fle:  mk_rekap.lgc
    $(CC) $(CFLAGS) appl=mk_rekap dotfle=$(TD)mk_rekap.fle
    
$(TD)mk_raz.fle:  mk_raz.lgc
    $(CC) $(CFLAGS) appl=mk_raz dotfle=$(TD)mk_raz.fle

$(TD)mk_pobr.fle:  mk_pobr.lgc
    $(CC) $(CFLAGS) appl=mk_pobr dotfle=$(TD)mk_pobr.fle
    
$(TD)mk_razin.fle:  mk_razin.lgc
    $(CC) $(CFLAGS) appl=mk_razin dotfle=$(TD)mk_razin.fle
    
$(TD)pregprod.fle:  pregprod.lgc
    $(CC) $(CFLAGS) appl=pregprod dotfle=$(TD)pregprod.fle
    
$(TD)lproart.fle:  lproart.lgc
    $(CC) $(CFLAGS) appl=lproart dotfle=$(TD)lproart.fle
    
$(TD)lprokup.fle:  lprokup.lgc
    $(CC) $(CFLAGS) appl=lprokup dotfle=$(TD)lprokup.fle
    
$(TD)mk_rntr.fle:  mk_rntr.lgc
    $(CC) $(CFLAGS) appl=mk_rntr dotfle=$(TD)mk_rntr.fle
    
$(TD)pregnab.fle:  pregnab.lgc
    $(CC) $(CFLAGS) appl=pregnab dotfle=$(TD)pregnab.fle
    
$(TD)lnabart.fle:  lnabart.lgc
    $(CC) $(CFLAGS) appl=lnabart dotfle=$(TD)lnabart.fle
    
$(TD)lnabdob.fle:  lnabdob.lgc
    $(CC) $(CFLAGS) appl=lnabdob dotfle=$(TD)lnabdob.fle
    
$(TD)mk_dobkt.fle:  mk_dobkt.lgc
    $(CC) $(CFLAGS) appl=mk_dobkt dotfle=$(TD)mk_dobkt.fle
    
$(TD)lm_dobkt.fle:  lm_dobkt.lgc
    $(CC) $(CFLAGS) appl=lm_dobkt dotfle=$(TD)lm_dobkt.fle
    
$(TD)mk_proci.fle:  mk_proci.lgc
    $(CC) $(CFLAGS) appl=mk_proci dotfle=$(TD)mk_proci.fle
    
$(TD)mk_izdrn.fle:  mk_izdrn.lgc
    $(CC) $(CFLAGS) appl=mk_izdrn dotfle=$(TD)mk_izdrn.fle
    
$(TD)rstanje.fle:  rstanje.lgc
    $(CC) $(CFLAGS) appl=rstanje dotfle=$(TD)rstanje.fle
    
$(TD)rkart.fle:  rkart.lgc
    $(CC) $(CFLAGS) appl=rkart dotfle=$(TD)rkart.fle
    
$(TD)lmsk_obr.fle:  lmsk_obr.lgc
    $(CC) $(CFLAGS) appl=lmsk_obr dotfle=$(TD)lmsk_obr.fle
    
$(TD)lmskpobr.fle:  lmskpobr.lgc
    $(CC) $(CFLAGS) appl=lmskpobr dotfle=$(TD)lmskpobr.fle
    
$(TD)lotpkalk.fle:  lotpkalk.lgc
    $(CC) $(CFLAGS) appl=lotpkalk dotfle=$(TD)lotpkalk.fle
    
$(TD)mksigkol.fle:  mksigkol.lgc
    $(CC) $(CFLAGS) appl=mksigkol dotfle=$(TD)mksigkol.fle
    
$(TD)lsigkol.fle:  lsigkol.lgc
    $(CC) $(CFLAGS) appl=lsigkol dotfle=$(TD)lsigkol.fle
    
$(TD)rsigkol.fle:  rsigkol.lgc
    $(CC) $(CFLAGS) appl=rsigkol dotfle=$(TD)rsigkol.fle
    
$(TD)lmkrekap.fle:  lmkrekap.lgc
    $(CC) $(CFLAGS) appl=lmkrekap dotfle=$(TD)lmkrekap.fle
    
$(TD)mk_svekt.fle:  mk_svekt.lgc
    $(CC) $(CFLAGS) appl=mk_svekt dotfle=$(TD)mk_svekt.fle
    
$(TD)mk_liman.fle:  mk_liman.lgc
    $(CC) $(CFLAGS) appl=mk_liman dotfle=$(TD)mk_liman.fle
    
$(TD)mpregkup.fle:  mpregkup.lgc
    $(CC) $(CFLAGS) appl=mpregkup dotfle=$(TD)mpregkup.fle

$(TD)mpregnab.fle:  mpregnab.lgc
    $(CC) $(CFLAGS) appl=mpregnab dotfle=$(TD)mpregnab.fle

$(TD)lkalkmal.fle:  lkalkmal.lgc
    $(CC) $(CFLAGS) appl=lkalkmal dotfle=$(TD)lkalkmal.fle

$(TD)mkimazal.fle:  mkimazal.lgc
    $(CC) $(CFLAGS) appl=mkimazal dotfle=$(TD)mkimazal.fle

$(TD)l_nard.fle:  l_nard.lgc
    $(CC) $(CFLAGS) appl=l_nard dotfle=$(TD)l_nard.fle

$(TD)lpc_nard.fle:  lpc_nard.lgc
    $(CC) $(CFLAGS) appl=lpc_nard dotfle=$(TD)lpc_nard.fle

$(TD)alfafiat.fle:  alfafiat.lgc
    $(CC) $(CFLAGS) appl=alfafiat dotfle=$(TD)alfafiat.fle

$(TD)lnprkup.fle:  lnprkup.lgc
    $(CC) $(CFLAGS) appl=lnprkup dotfle=$(TD)lnprkup.fle

$(TD)malprim.fle:  malprim.lgc
    $(CC) $(CFLAGS) appl=malprim dotfle=$(TD)malprim.fle

$(TD)velprim.fle:  velprim.lgc
    $(CC) $(CFLAGS) appl=velprim dotfle=$(TD)velprim.fle

$(TD)mkchkcje.fle:  mkchkcje.lgc
    $(CC) $(CFLAGS) appl=mkchkcje dotfle=$(TD)mkchkcje.fle

$(TD)cartinfo.fle:  cartinfo.lgc
    $(CC) $(CFLAGS) appl=cartinfo dotfle=$(TD)cartinfo.fle
    $(CC) $(CFLAGS) appl=cartinfo dotfle=$(TD)kat_cjenik.fle

$(TD)cartgen.fle:  cartgen.lgc
    $(CC) $(CFLAGS) appl=cartgen dotfle=$(TD)cartgen.fle

$(TD)mk_top.fle:  mk_top.lgc
    $(CC) $(CFLAGS) appl=mk_top dotfle=$(TD)mk_top.fle

$(TD)lrek_kol.fle:  lrek_kol.lgc
    $(CC) $(CFLAGS) appl=lrek_kol dotfle=$(TD)lrek_kol.fle

$(TD)l_invlst.fle:  l_invlst.lgc
    $(CC) $(CFLAGS) appl=l_invlst dotfle=$(TD)l_invlst.fle

$(TD)lotpart.fle:  lotpart.lgc
    $(CC) $(CFLAGS) appl=lotpart dotfle=$(TD)lotpart.fle

$(TD)lmskart.fle:  lmskart.lgc
    $(CC) $(CFLAGS) appl=lmskart dotfle=$(TD)lmskart.fle

$(TD)lotpkup.fle:  lotpkup.lgc
    $(CC) $(CFLAGS) appl=lotpkup dotfle=$(TD)lotpkup.fle

$(TD)lmskkup.fle:  lmskkup.lgc
    $(CC) $(CFLAGS) appl=lmskkup dotfle=$(TD)lmskkup.fle

$(TD)lfakt_izv.fle:  lfakt_izv.lgc
    $(CC) $(CFLAGS) appl=lfakt_izv dotfle=$(TD)lfakt_izv.fle

$(TD)fladbutl.fle:  fladbutl.lgc
    $(CC) $(CFLAGS) appl=fladbutl dotfle=$(TD)fladbutl.fle

$(TD)lprodrab.fle:  lprodrab.lgc
    $(CC) $(CFLAGS) appl=lprodrab dotfle=$(TD)lprodrab.fle

$(TD)lprodtip.fle:  lprodtip.lgc
    $(CC) $(CFLAGS) appl=lprodtip dotfle=$(TD)lprodtip.fle

$(TD)komesto.fle:  komesto.lgc
    $(CC) $(CFLAGS) appl=komesto dotfle=$(TD)komesto.fle

$(TD)vrmkdok.fle:  vrmkdok.lgc
    $(CC) $(CFLAGS) appl=vrmkdok dotfle=$(TD)vrmkdok.fle

$(TD)mat_tecaj.fle:  mat_tecaj.lgc
    $(CC) $(CFLAGS) appl=mat_tecaj dotfle=$(TD)mat_tecaj.fle

$(TD)mkvolksdbf.fle:  mkvolksdbf.lgc
    $(CC) $(CFLAGS) appl=mkvolksdbf dotfle=$(TD)mkvolksdbf.fle

$(TD)mkaddotps.fle:  mkaddotps.lgc
    $(CC) $(CFLAGS) appl=mkaddotps dotfle=$(TD)mkaddotps.fle

$(TD)mkaddprims.fle:  mkaddprims.lgc
    $(CC) $(CFLAGS) appl=mkaddprims dotfle=$(TD)mkaddprims.fle

$(TD)vd_podanima.fle:  vd_podanima.lgc
    $(CC) $(CFLAGS) appl=vd_podanima dotfle=$(TD)vd_podanima.fle

$(TD)leasingst.fle:  leasingst.lgc
    $(CC) $(CFLAGS) appl=leasingst dotfle=$(TD)leasingst.fle

$(TD)nk_grupe.fle:  nk_grupe.lgc
    $(CC) $(CFLAGS) appl=nk_grupe dotfle=$(TD)nk_grupe.fle

$(TD)rns_grfakart.fle:  rns_grfakart.lgc
    $(CC) $(CFLAGS) appl=rns_grfakart dotfle=$(TD)rns_grfakart.fle

$(TD)mkaddmsks.fle:  mkaddmsks.lgc
    $(CC) $(CFLAGS) appl=mkaddmsks dotfle=$(TD)mkaddmsks.fle

$(TD)lnd_rast.fle:  lnd_rast.lgc
    $(CC) $(CFLAGS) appl=lnd_rast dotfle=$(TD)lnd_rast.fle

$(TD)mat_default.fle:  mat_default.lgc
    $(CC) $(CFLAGS) appl=mat_default dotfle=$(TD)mat_default.fle

$(TD)mpregMSK.fle:  mpregMSK.lgc
    $(CC) $(CFLAGS) appl=mpregMSK dotfle=$(TD)mpregMSK.fle

$(TD)rns_allrn.fle:  rns_allrn.lgc
    $(CC) $(CFLAGS) appl=rns_allrn dotfle=$(TD)rns_allrn.fle

$(TD)AC_rn_allrek.fle:  AC_rn_allrek.lgc
    $(CC) $(CFLAGS) appl=AC_rn_allrek dotfle=$(TD)AC_rn_allrek.fle

$(TD)nf_otp.fle:  nf_otp.lgc
    $(CC) $(CFLAGS) appl=nf_otp dotfle=$(TD)nf_otp.fle

$(TD)rns_allup.fle:  rns_allup.lgc
    $(CC) $(CFLAGS) appl=rns_allup dotfle=$(TD)rns_allup.fle

$(TD)lmk_zamjene.fle:  lmk_zamjene.lgc
    $(CC) $(CFLAGS) appl=lmk_zamjene dotfle=$(TD)lmk_zamjene.fle

$(TD)l_zalart.fle:  l_zalart.lgc
    $(CC) $(CFLAGS) appl=l_zalart dotfle=$(TD)l_zalart.fle

$(TD)l_zalkup.fle:  l_zalkup.lgc
    $(CC) $(CFLAGS) appl=l_zalkup dotfle=$(TD)l_zalkup.fle

$(TD)lrd_cjenvp.fle:  lrd_cjenvp.lgc
    $(CC) $(CFLAGS) appl=lrd_cjenvp dotfle=$(TD)lrd_cjenvp.fle

$(TD)rn_poslovi.fle:  rn_poslovi.lgc
    $(CC) $(CFLAGS) appl=rn_poslovi dotfle=$(TD)rn_poslovi.fle

$(TD)rn_predlozak.fle:  rn_predlozak.lgc
    $(CC) $(CFLAGS) appl=rn_predlozak dotfle=$(TD)rn_predlozak.fle

$(TD)mk_zapisnik.fle:  mk_zapisnik.lgc
    $(CC) $(CFLAGS) appl=mk_zapisnik dotfle=$(TD)mk_zapisnik.fle

$(TD)lmk_zap.fle:  lmk_zap.lgc
    $(CC) $(CFLAGS) appl=lmk_zap dotfle=$(TD)lmk_zap.fle

$(TD)rns_modvoz.fle:  rns_modvoz.lgc
    $(CC) $(CFLAGS) appl=rns_modvoz dotfle=$(TD)rns_modvoz.fle

$(TD)rn_fakart.fle:  rn_fakart.lgc
    $(CC) $(CFLAGS) appl=rn_fakart dotfle=$(TD)rn_fakart.fle

$(TD)altnaz_fakart.fle:  altnaz_fakart.lgc
    $(CC) $(CFLAGS) appl=altnaz_fakart dotfle=$(TD)altnaz_fakart.fle

$(TD)faktureRN.fle:  faktureRN.lgc
    $(CC) $(CFLAGS) appl=faktureRN dotfle=$(TD)faktureRN.fle

$(TD)mk_nardug.fle:  mk_nardug.lgc
    $(CC) $(CFLAGS) appl=mk_nardug dotfle=$(TD)mk_nardug.fle

$(TD)lrnvozkart.fle:  lrnvozkart.lgc
    $(CC) $(CFLAGS) appl=lrnvozkart dotfle=$(TD)lrnvozkart.fle

$(TD)mk_prmp.fle:  mk_prmp.lgc
    $(CC) $(CFLAGS) appl=mk_prmp dotfle=$(TD)mk_prmp.fle

$(TD)l_radrnal.fle:  l_radrnal.lgc
    $(CC) $(CFLAGS) appl=l_radrnal dotfle=$(TD)l_radrnal.fle

$(TD)l_radrnal_am.fle:  l_radrnal_am.lgc
    $(CC) $(CFLAGS) appl=l_radrnal_am dotfle=$(TD)l_radrnal_am.fle

$(TD)lrns_potrosni.fle:  lrns_potrosni.lgc
    $(CC) $(CFLAGS) appl=lrns_potrosni dotfle=$(TD)lrns_potrosni.fle

$(TD)l_radrnal_AC.fle:  l_radrnal_AC.lgc
    $(CC) $(CFLAGS) appl=l_radrnal_AC dotfle=$(TD)l_radrnal_AC.fle

$(TD)l_rekrnal_AC.fle:  l_rekrnal_AC.lgc
    $(CC) $(CFLAGS) appl=l_rekrnal_AC dotfle=$(TD)l_rekrnal_AC.fle

$(TD)lnabprim.fle:  lnabprim.lgc
    $(CC) $(CFLAGS) appl=lnabprim dotfle=$(TD)lnabprim.fle

$(TD)m_familije.fle:  m_familije.lgc
    $(CC) $(CFLAGS) appl=m_familije dotfle=$(TD)m_familije.fle

$(TD)unosprimke.fle:  unosprimke.lgc
    $(CC) $(CFLAGS) appl=unosprimke dotfle=$(TD)unosprimke.fle

$(TD)unosprimke_bt.fle:  unosprimke_bt.lgc
    $(CC) $(CFLAGS) appl=unosprimke_bt dotfle=$(TD)unosprimke_bt.fle

$(TD)l_nardug.fle:  l_nardug.lgc
    $(CC) $(CFLAGS) appl=l_nardug dotfle=$(TD)l_nardug.fle

$(TD)mk_dugvp.fle:  mk_dugvp.lgc
    $(CC) $(CFLAGS) appl=mk_dugvp dotfle=$(TD)mk_dugvp.fle

$(TD)lrnobrlst.fle:  lrnobrlst.lgc
    $(CC) $(CFLAGS) appl=lrnobrlst dotfle=$(TD)lrnobrlst.fle

$(TD)l_poprn.fle:  l_poprn.lgc
    $(CC) $(CFLAGS) appl=l_poprn dotfle=$(TD)l_poprn.fle

$(TD)l_m_vrnal.fle:  l_m_vrnal.lgc
    $(CC) $(CFLAGS) appl=l_m_vrnal dotfle=$(TD)l_m_vrnal.fle

$(TD)l_vrmkdok.fle:  l_vrmkdok.lgc
    $(CC) $(CFLAGS) appl=l_vrmkdok dotfle=$(TD)l_vrmkdok.fle

$(TD)l_intfakt.fle:  l_intfakt.lgc
    $(CC) $(CFLAGS) appl=l_intfakt dotfle=$(TD)l_intfakt.fle

$(TD)l_intfaktr.fle:  l_intfaktr.lgc
    $(CC) $(CFLAGS) appl=l_intfaktr dotfle=$(TD)l_intfaktr.fle

$(TD)mk_obrtaj.fle:  mk_obrtaj.lgc
    $(CC) $(CFLAGS) appl=mk_obrtaj dotfle=$(TD)mk_obrtaj.fle

$(TD)lmk_obrtaj.fle:  lmk_obrtaj.lgc
    $(CC) $(CFLAGS) appl=lmk_obrtaj dotfle=$(TD)lmk_obrtaj.fle

$(TD)l_mprnr1.fle:  l_mprnr1.lgc
    $(CC) $(CFLAGS) appl=l_mprnr1 dotfle=$(TD)l_mprnr1.fle

$(TD)l_matrn.fle:  l_matrn.lgc
    $(CC) $(CFLAGS) appl=l_matrn dotfle=$(TD)l_matrn.fle

$(TD)lskol2.fle:  lskol2.lgc
    $(CC) $(CFLAGS) appl=lskol2 dotfle=$(TD)lskol2.fle

$(TD)lmk_rekz.fle:  lmk_rekz.lgc
    $(CC) $(CFLAGS) appl=lmk_rekz dotfle=$(TD)lmk_rekz.fle

$(TD)l_matart.fle:  l_matart.lgc
    $(CC) $(CFLAGS) appl=l_matart dotfle=$(TD)l_matart.fle

$(TD)r_reexp.fle:  r_reexp.lgc
    $(CC) $(CFLAGS) appl=r_reexp dotfle=$(TD)r_reexp.fle

$(TD)pack_list.fle:  pack_list.lgc
    $(CC) $(CFLAGS) appl=pack_list dotfle=$(TD)pack_list.fle

$(TD)r_rac_eur.fle:  r_rac_eur.lgc
    $(CC) $(CFLAGS) appl=r_rac_eur dotfle=$(TD)r_rac_eur.fle

$(TD)r_rac_sit.fle:  r_rac_sit.lgc
    $(CC) $(CFLAGS) appl=r_rac_sit dotfle=$(TD)r_rac_sit.fle

$(TD)lrns_fakt.fle:  lrns_fakt.lgc
    $(CC) $(CFLAGS) appl=lrns_fakt dotfle=$(TD)lrns_fakt.fle

$(TD)lrns_faktam.fle:  lrns_faktam.lgc
    $(CC) $(CFLAGS) appl=lrns_faktam dotfle=$(TD)lrns_faktam.fle

$(TD)lrns_fakt_ml.fle:  lrns_fakt_ml.lgc
    $(CC) $(CFLAGS) appl=lrns_fakt_ml dotfle=$(TD)lrns_fakt_ml.fle

$(TD)lrns_zap_ml.fle:  lrns_zap_ml.lgc
    $(CC) $(CFLAGS) appl=lrns_zap_ml dotfle=$(TD)lrns_zap_ml.fle

$(TD)rns_dorada.fle:  rns_dorada.lgc
    $(CC) $(CFLAGS) appl=rns_dorada dotfle=$(TD)rns_dorada.fle

$(TD)rns_doraraz.fle:  rns_doraraz.lgc
    $(CC) $(CFLAGS) appl=rns_doraraz dotfle=$(TD)rns_doraraz.fle

$(TD)mk_najam.fle:  mk_najam.lgc
    $(CC) $(CFLAGS) appl=mk_najam dotfle=$(TD)mk_najam.fle

$(TD)mk_najflote.fle:  mk_najflote.lgc
    $(CC) $(CFLAGS) appl=mk_najflote dotfle=$(TD)mk_najflote.fle

$(TD)ml_kartice.fle:  ml_kartice.lgc
    $(CC) $(CFLAGS) appl=ml_kartice dotfle=$(TD)ml_kartice.fle

$(TD)lml_kartice.fle:  lml_kartice.lgc
    $(CC) $(CFLAGS) appl=lml_kartice dotfle=$(TD)lml_kartice.fle

$(TD)ml_kartnaj.fle:  ml_kartnaj.lgc
    $(CC) $(CFLAGS) appl=ml_kartnaj dotfle=$(TD)ml_kartnaj.fle

$(TD)ml_kfaknaj.fle:  ml_kfaknaj.lgc
    $(CC) $(CFLAGS) appl=ml_kfaknaj dotfle=$(TD)ml_kfaknaj.fle

$(TD)ml_kservis.fle:  ml_kservis.lgc
    $(CC) $(CFLAGS) appl=ml_kservis dotfle=$(TD)ml_kservis.fle

$(TD)ml_kartall.fle:  ml_kartall.lgc
    $(CC) $(CFLAGS) appl=ml_kartall dotfle=$(TD)ml_kartall.fle

$(TD)lml_kartall.fle:  lml_kartall.lgc
    $(CC) $(CFLAGS) appl=lml_kartall dotfle=$(TD)lml_kartall.fle

$(TD)ml_kart_old.fle:  ml_kart_old.lgc
    $(CC) $(CFLAGS) appl=ml_kart_old dotfle=$(TD)ml_kart_old.fle

$(TD)lrazlikecd.fle:  lrazlikecd.lgc
    $(CC) $(CFLAGS) appl=lrazlikecd dotfle=$(TD)lrazlikecd.fle

$(TD)lnajam_zap.fle:  lnajam_zap.lgc
    $(CC) $(CFLAGS) appl=lnajam_zap dotfle=$(TD)lnajam_zap.fle

$(TD)mk_najam_st.fle:  mk_najam_st.lgc
    $(CC) $(CFLAGS) appl=mk_najam_st dotfle=$(TD)mk_najam_st.fle

$(TD)lmk_najam.fle:  lmk_najam.lgc
    $(CC) $(CFLAGS) appl=lmk_najam dotfle=$(TD)lmk_najam.fle

$(TD)najam_cppar.fle:  najam_cppar.lgc
    $(CC) $(CFLAGS) appl=najam_cppar dotfle=$(TD)najam_cppar.fle

$(TD)najam_fakture.fle:  najam_fakture.lgc
    $(CC) $(CFLAGS) appl=najam_fakture dotfle=$(TD)najam_fakture.fle

$(TD)mk_natjecaj.fle:  mk_natjecaj.lgc
    $(CC) $(CFLAGS) appl=mk_natjecaj dotfle=$(TD)mk_natjecaj.fle

$(TD)mk_natcjenik.fle:  mk_natcjenik.lgc
    $(CC) $(CFLAGS) appl=mk_natcjenik dotfle=$(TD)mk_natcjenik.fle

$(TD)lmk_natcjenik.fle:  lmk_natcjenik.lgc
    $(CC) $(CFLAGS) appl=lmk_natcjenik dotfle=$(TD)lmk_natcjenik.fle

$(TD)mk_usteda.fle:  mk_usteda.lgc
    $(CC) $(CFLAGS) appl=mk_usteda dotfle=$(TD)mk_usteda.fle

$(TD)mk_cjenugo.fle:  mk_cjenugo.lgc
    $(CC) $(CFLAGS) appl=mk_cjenugo dotfle=$(TD)mk_cjenugo.fle

$(TD)mk_cjenppar.fle:  mk_cjenppar.lgc
    $(CC) $(CFLAGS) appl=mk_cjenppar dotfle=$(TD)mk_cjenppar.fle

$(TD)lmk_cjenppar.fle:  lmk_cjenppar.lgc
    $(CC) $(CFLAGS) appl=lmk_cjenppar dotfle=$(TD)lmk_cjenppar.fle

$(TD)lmk_usteda.fle:  lmk_usteda.lgc
    $(CC) $(CFLAGS) appl=lmk_usteda dotfle=$(TD)lmk_usteda.fle

$(TD)vd_sdok.fle:  vd_sdok.lgc
    $(CC) $(CFLAGS) appl=vd_sdok dotfle=$(TD)vd_sdok.fle

$(TD)mk_najamfak.fle:  mk_najamfak.lgc
    $(CC) $(CFLAGS) appl=mk_najamfak dotfle=$(TD)mk_najamfak.fle

$(TD)mk_promnajam.fle:  mk_promnajam.lgc
    $(CC) $(CFLAGS) appl=mk_promnajam dotfle=$(TD)mk_promnajam.fle

$(TD)lmk_promnajam.fle:  lmk_promnajam.lgc
    $(CC) $(CFLAGS) appl=lmk_promnajam dotfle=$(TD)lmk_promnajam.fle

$(TD)mk_faknajam.fle:  mk_faknajam.lgc
    $(CC) $(CFLAGS) appl=mk_faknajam dotfle=$(TD)mk_faknajam.fle

$(TD)azur_faknajam.fle:  azur_faknajam.lgc
    $(CC) $(CFLAGS) appl=azur_faknajam dotfle=$(TD)azur_faknajam.fle

$(TD)mk_azurotp.fle:  mk_azurotp.lgc
    $(CC) $(CFLAGS) appl=mk_azurotp dotfle=$(TD)mk_azurotp.fle

$(TD)l_chg_cje.fle:  l_chg_cje.lgc
    $(CC) $(CFLAGS) appl=l_chg_cje dotfle=$(TD)l_chg_cje.fle

$(TD)lm_familije.fle:  lm_familije.lgc
    $(CC) $(CFLAGS) appl=lm_familije dotfle=$(TD)lm_familije.fle

$(TD)lmpr_zap.fle:  lmpr_zap.lgc
    $(CC) $(CFLAGS) appl=lmpr_zap dotfle=$(TD)lmpr_zap.fle

$(TD)lmktsmje.fle:  lmktsmje.lgc
    $(CC) $(CFLAGS) appl=lmktsmje dotfle=$(TD)lmktsmje.fle

$(TD)lmpr_konlp.fle:  lmpr_konlp.lgc
    $(CC) $(CFLAGS) appl=lmpr_konlp dotfle=$(TD)lmpr_konlp.fle

$(TD)msk_preg.fle:  msk_preg.lgc
    $(CC) $(CFLAGS) appl=msk_preg dotfle=$(TD)msk_preg.fle

$(TD)rnsplaner.fle:  rnsplaner.lgc
    $(CC) $(CFLAGS) appl=rnsplaner dotfle=$(TD)rnsplaner.fle

$(TD)l_rnsplaner.fle:  l_rnsplaner.lgc
    $(CC) $(CFLAGS) appl=l_rnsplaner dotfle=$(TD)l_rnsplaner.fle

$(TD)lmsk_rast.fle:  lmsk_rast.lgc
    $(CC) $(CFLAGS) appl=lmsk_rast dotfle=$(TD)lmsk_rast.fle

$(TD)lmk_rekcij.fle:  lmk_rekcij.lgc
    $(CC) $(CFLAGS) appl=lmk_rekcij dotfle=$(TD)lmk_rekcij.fle

$(TD)mt_invzap.fle:  mt_invzap.lgc
    $(CC) $(CFLAGS) appl=mt_invzap dotfle=$(TD)mt_invzap.fle

$(TD)mk_prijelaz.fle:  mk_prijelaz.lgc
    $(CC) $(CFLAGS) appl=mk_prijelaz dotfle=$(TD)mk_prijelaz.fle

$(TD)mk_nedovr.fle:  mk_nedovr.lgc
    $(CC) $(CFLAGS) appl=mk_nedovr dotfle=$(TD)mk_nedovr.fle

$(TD)mk_nedser.fle:  mk_nedser.lgc
    $(CC) $(CFLAGS) appl=mk_nedser dotfle=$(TD)mk_nedser.fle

$(TD)rns_faktmp.fle:  rns_faktmp.lgc
    $(CC) $(CFLAGS) appl=rns_faktmp dotfle=$(TD)rns_faktmp.fle

$(TD)rns_fakppar.fle:  rns_fakppar.lgc
    $(CC) $(CFLAGS) appl=rns_fakppar dotfle=$(TD)rns_fakppar.fle

$(TD)rns_stfaktmp.fle:  rns_stfaktmp.lgc
    $(CC) $(CFLAGS) appl=rns_stfaktmp dotfle=$(TD)rns_stfaktmp.fle

$(TD)lrd_cvpde.fle:  lrd_cvpde.lgc
    $(CC) $(CFLAGS) appl=lrd_cvpde dotfle=$(TD)lrd_cvpde.fle

$(TD)util_ameh.fle:  util_ameh.lgc
    $(CC) $(CFLAGS) appl=util_ameh dotfle=$(TD)util_ameh.fle

$(TD)mkobmsk2.fle:  mkobmsk2.lgc
    $(CC) $(CFLAGS) appl=mkobmsk2 dotfle=$(TD)mkobmsk2.fle

$(TD)lmsk2_obr.fle:  lmsk2_obr.lgc
    $(CC) $(CFLAGS) appl=lmsk2_obr dotfle=$(TD)lmsk2_obr.fle

$(TD)lneomsk2.fle:  lneomsk2.lgc
    $(CC) $(CFLAGS) appl=lneomsk2 dotfle=$(TD)lneomsk2.fle

$(TD)mksklmsk2.fle:  mksklmsk2.lgc
    $(CC) $(CFLAGS) appl=mksklmsk2 dotfle=$(TD)mksklmsk2.fle

$(TD)mkinvrt.fle:  mkinvrt.lgc
    $(CC) $(CFLAGS) appl=mkinvrt dotfle=$(TD)mkinvrt.fle

$(TD)deal_export.fle:  deal_export.lgc
    $(CC) $(CFLAGS) appl=deal_export dotfle=$(TD)deal_export.fle

$(TD)rnsvlas_AMEH.fle:  rnsvlas_AMEH.lgc
    $(CC) $(CFLAGS) appl=rnsvlas_AMEH dotfle=$(TD)rnsvlas_AMEH.fle

$(TD)rns_mjizvj.fle:  rns_mjizvj.lgc
    $(CC) $(CFLAGS) appl=rns_mjizvj dotfle=$(TD)rns_mjizvj.fle

$(TD)lvoz_primVA.fle:  lvoz_primVA.lgc
    $(CC) $(CFLAGS) appl=lvoz_primVA dotfle=$(TD)lvoz_primVA.fle

$(TD)rac_vpmp.fle:  rac_vpmp.lgc
    $(CC) $(CFLAGS) appl=rac_vpmp dotfle=$(TD)rac_vpmp.fle

$(TD)rac_vpmpst.fle:  rac_vpmpst.lgc
    $(CC) $(CFLAGS) appl=rac_vpmpst dotfle=$(TD)rac_vpmpst.fle

$(TD)vd_msk.fle:  vd_msk.lgc
    $(CC) $(CFLAGS) appl=vd_msk dotfle=$(TD)vd_msk.fle

$(TD)lvd_msk.fle:  lvd_msk.lgc
    $(CC) $(CFLAGS) appl=lvd_msk dotfle=$(TD)lvd_msk.fle

$(TD)lvd_pvmsk.fle:  lvd_pvmsk.lgc
    $(CC) $(CFLAGS) appl=lvd_pvmsk dotfle=$(TD)lvd_pvmsk.fle

$(TD)vd_postoruc.fle:  vd_postoruc.lgc
    $(CC) $(CFLAGS) appl=vd_postoruc dotfle=$(TD)vd_postoruc.fle

$(TD)vdmskst.fle:  vdmskst.lgc
    $(CC) $(CFLAGS) appl=vdmskst dotfle=$(TD)vdmskst.fle

$(TD)lvd_ulkzap.fle:  lvd_ulkzap.lgc
    $(CC) $(CFLAGS) appl=lvd_ulkzap dotfle=$(TD)lvd_ulkzap.fle

$(TD)lvd_izkzap.fle:  lvd_izkzap.lgc
    $(CC) $(CFLAGS) appl=lvd_izkzap dotfle=$(TD)lvd_izkzap.fle

$(TD)lvd_zbizkzap.fle:  lvd_zbizkzap.lgc
    $(CC) $(CFLAGS) appl=lvd_zbizkzap dotfle=$(TD)lvd_zbizkzap.fle


$(TD)vd_kontkzap.fle:  vd_kontkzap.lgc
    $(CC) $(CFLAGS) appl=vd_kontkzap dotfle=$(TD)vd_kontkzap.fle

$(TD)vd_nab.fle:  vd_nab.lgc
    $(CC) $(CFLAGS) appl=vd_nab dotfle=$(TD)vd_nab.fle

$(TD)lvd_nab.fle:  lvd_nab.lgc
    $(CC) $(CFLAGS) appl=lvd_nab dotfle=$(TD)lvd_nab.fle

$(TD)vd_pvmsks.fle:  vd_pvmsks.lgc
    $(CC) $(CFLAGS) appl=vd_pvmsks dotfle=$(TD)vd_pvmsks.fle

$(TD)vd_pvmsk.fle:  vd_pvmsk.lgc
    $(CC) $(CFLAGS) appl=vd_pvmsk dotfle=$(TD)vd_pvmsk.fle

$(TD)vd_dorada.fle:  vd_dorada.lgc
    $(CC) $(CFLAGS) appl=vd_dorada dotfle=$(TD)vd_dorada.fle

$(TD)vd_dor_int.fle:  vd_dor_int.lgc
    $(CC) $(CFLAGS) appl=vd_dor_int dotfle=$(TD)vd_dor_int.fle

$(TD)vd_prmp.fle:  vd_prmp.lgc
    $(CC) $(CFLAGS) appl=vd_prmp dotfle=$(TD)vd_prmp.fle

$(TD)vd_prmp_ppar.fle:  vd_prmp_ppar.lgc
    $(CC) $(CFLAGS) appl=vd_prmp_ppar dotfle=$(TD)vd_prmp_ppar.fle

$(TD)vd_rezerva.fle:  vd_rezerva.lgc
    $(CC) $(CFLAGS) appl=vd_rezerva dotfle=$(TD)vd_rezerva.fle

$(TD)lvd_razkzap.fle:  lvd_razkzap.lgc
    $(CC) $(CFLAGS) appl=lvd_razkzap dotfle=$(TD)lvd_razkzap.fle

$(TD)lvd_kzap_car.fle:  lvd_kzap_car.lgc
    $(CC) $(CFLAGS) appl=lvd_kzap_car dotfle=$(TD)lvd_kzap_car.fle

$(TD)l_vdcar_raz.fle:  l_vdcar_raz.lgc
    $(CC) $(CFLAGS) appl=l_vdcar_raz dotfle=$(TD)l_vdcar_raz.fle

$(TD)vd_popcar.fle:  vd_popcar.lgc
    $(CC) $(CFLAGS) appl=vd_popcar dotfle=$(TD)vd_popcar.fle

$(TD)extra_nar.fle:  extra_nar.lgc
    $(CC) $(CFLAGS) appl=extra_nar dotfle=$(TD)extra_nar.fle

$(TD)vd_salon.fle:  vd_salon.lgc
    $(CC) $(CFLAGS) appl=vd_salon dotfle=$(TD)vd_salon.fle

$(TD)vd_sklad_dor.fle:  vd_sklad_dor.lgc
    $(CC) $(CFLAGS) appl=vd_sklad_dor dotfle=$(TD)vd_sklad_dor.fle

$(TD)vd_jamstvo.fle:  vd_jamstvo.lgc
    $(CC) $(CFLAGS) appl=vd_jamstvo dotfle=$(TD)vd_jamstvo.fle

$(TD)arh_fiatcj.fle:  arh_fiatcj.lgc
    $(CC) $(CFLAGS) appl=arh_fiatcj dotfle=$(TD)arh_fiatcj.fle

$(TD)l_rns_radnik.fle:  l_rns_radnik.lgc
    $(CC) $(CFLAGS) appl=l_rns_radnik dotfle=$(TD)l_rns_radnik.fle

$(TD)rns_mob_otp.fle:  rns_mob_otp.lgc
    $(CC) $(CFLAGS) appl=rns_mob_otp dotfle=$(TD)rns_mob_otp.fle
    
$(TD)rns_mob_usl.fle:  rns_mob_usl.lgc
    $(CC) $(CFLAGS) appl=rns_mob_usl dotfle=$(TD)rns_mob_usl.fle
    
$(TD)rns_mob_rn.fle:  rns_mob_rn.lgc
    $(CC) $(CFLAGS) appl=rns_mob_rn dotfle=$(TD)rns_mob_rn.fle
    
$(TD)rns_mob.fle:  rns_mob.lgc
    $(CC) $(CFLAGS) appl=rns_mob dotfle=$(TD)rns_mob.fle
    
$(TD)rns_mob_ev.fle:  rns_mob_ev.lgc
    $(CC) $(CFLAGS) appl=rns_mob_ev dotfle=$(TD)rns_mob_ev.fle
    
$(TD)lrn_fakart.fle:  lrn_fakart.lgc
    $(CC) $(CFLAGS) appl=lrn_fakart dotfle=$(TD)lrn_fakart.fle

$(TD)mob_rnizd.fle:  mob_rnizd.lgc
    $(CC) $(CFLAGS) appl=mob_rnizd dotfle=$(TD)mob_rnizd.fle
    
$(TD)mat_rev.fle:  mat_rev.lgc
    $(CC) $(CFLAGS) appl=mat_rev dotfle=$(TD)mat_rev.fle

$(TD)mlugovori.fle:  mlugovori.lgc
    $(CC) $(CFLAGS) appl=mlugovori dotfle=$(TD)mlugovori.fle
    
$(TD)ml_cjenik.fle:  ml_cjenik.lgc
    $(CC) $(CFLAGS) appl=ml_cjenik dotfle=$(TD)ml_cjenik.fle
    
$(TD)l_ug_cjenik.fle:  l_ug_cjenik.lgc
    $(CC) $(CFLAGS) appl=l_ug_cjenik dotfle=$(TD)l_ug_cjenik.fle
    
$(TD)karic_mit.fle:  karic_mit.lgc
    $(CC) $(CFLAGS) appl=karic_mit dotfle=$(TD)karic_mit.fle

$(TD)lpv_ispod_nab.fle:  lpv_ispod_nab.lgc
    $(CC) $(CFLAGS) appl=lpv_ispod_nab dotfle=$(TD)lpv_ispod_nab.fle

$(TD)lmob_rn_izvj.fle:  lmob_rn_izvj.lgc
    $(CC) $(CFLAGS) appl=lmob_rn_izvj dotfle=$(TD)lmob_rn_izvj.fle

$(TD)stvilicari.fle:  stvilicari.lgc
    $(CC) $(CFLAGS) appl=stvilicari dotfle=$(TD)stvilicari.fle

$(TD)vd_crediflex.fle:  vd_crediflex.lgc
    $(CC) $(CFLAGS) appl=vd_crediflex dotfle=$(TD)vd_crediflex.fle

$(TD)lmk_nabava.fle:  lmk_nabava.lgc
    $(CC) $(CFLAGS) appl=lmk_nabava dotfle=$(TD)lmk_nabava.fle

$(TD)lmk_otprema.fle:  lmk_otprema.lgc
    $(CC) $(CFLAGS) appl=lmk_otprema dotfle=$(TD)lmk_otprema.fle

$(TD)pv_npl.fle:  pv_npl.lgc
    $(CC) $(CFLAGS) appl=pv_npl dotfle=$(TD)pv_npl.fle

$(TD)faktusl_as.fle:  faktusl_as.lgc
    $(CC) $(CFLAGS) appl=faktusl_as dotfle=$(TD)faktusl_as.fle

$(TD)lfaktusl_as.fle:  lfaktusl_as.lgc
    $(CC) $(CFLAGS) appl=lfaktusl_as dotfle=$(TD)lfaktusl_as.fle

$(TD)lpopiskol.fle:  lpopiskol.lgc
    $(CC) $(CFLAGS) appl=lpopiskol dotfle=$(TD)lpopiskol.fle

$(TD)rns_boje.fle:  rns_boje.lgc
    $(CC) $(CFLAGS) appl=rns_boje dotfle=$(TD)rns_boje.fle

#########################################################################
#
# RAD DEALERA SA AC ZAGREB
#
#########################################################################

$(TD)deac.fle:  deac.lgc
    $(CC) $(CFLAGS) appl=deac dotfle=$(TD)deac.fle

$(TD)deac_prot.fle:  deac_prot.lgc
    $(CC) $(CFLAGS) appl=deac_prot dotfle=$(TD)deac_prot.fle

$(TD)deac_prnd.fle:  deac_prnd.lgc
    $(CC) $(CFLAGS) appl=deac_prnd dotfle=$(TD)deac_prnd.fle

$(TD)deacoff.fle:  deacoff.lgc
    $(CC) $(CFLAGS) appl=deacoff dotfle=$(TD)deacoff.fle

$(TD)deacoffot.fle:  deacoffot.lgc
    $(CC) $(CFLAGS) appl=deacoffot dotfle=$(TD)deacoffot.fle

$(TD)deacoffnd.fle:  deacoffnd.lgc
    $(CC) $(CFLAGS) appl=deacoffnd dotfle=$(TD)deacoffnd.fle

$(TD)acde_stanoff.fle:  acde_stanoff.lgc
    $(CC) $(CFLAGS) appl=acde_stanoff dotfle=$(TD)acde_stanoff.fle

$(TD)kat_cjenik.fle:  kat_cjenik.lgc
    $(CC) $(CFLAGS) appl=kat_cjenik dotfle=$(TD)kat_cjenik.fle

$(TD)ameh_ps.fle:  ameh_ps.lgc
    $(CC) $(CFLAGS) appl=ameh_ps dotfle=$(TD)ameh_ps.fle

$(TD)bertol_ps.fle:  bertol_ps.lgc
    $(CC) $(CFLAGS) appl=bertol_ps dotfle=$(TD)bertol_ps.fle

$(TD)aca_temp.fle:  aca_temp.lgc
    $(CC) $(CFLAGS) appl=aca_temp dotfle=$(TD)aca_temp.fle

$(TD)acatempdd.fle:  acatempdd.lgc
    $(CC) $(CFLAGS) appl=acatempdd dotfle=$(TD)acatempdd.fle
